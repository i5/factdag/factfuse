const path = require("path");
const CopyPlugin = require("copy-webpack-plugin");

module.exports = {
  mode: process.NODE_ENV || "development",
  entry: {
    provView: "./src/UI/provView.ts",
    revisionView: "./src/UI/RevisionView.ts",
    activityView: "./src/UI/ActivityView.ts",
    commitGUI: "./src/UI/CommitGUI.ts",
    initGUI: "./src/UI/InitGUI.ts",
    index: "./src/UI/Index.ts",
    dialogOpener: "./src/UI/dialogOpener.ts",
    tutorial: "./src/UI/tutorial.ts"
  },
  target: "node",
  watch: false,
  output: {
    path: path.resolve(__dirname, "./dist"),
    filename: "[name].js"
  },
  node: {
    __filename: false,
    __dirname: false,
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: "ts-loader",
        exclude: /node_modules/
      },
      {
        test: /\.(png|jpe?g|gif|svg)$/i,
        use: [
          {
            loader: "file-loader",
            options: { publicPath: "dist" }
          }
        ]
      },
      {
        test: /\.node$/,
        use: [
          {
            loader: "node-loader",
            options: { name: "[name]-[hash].[ext]" }
          }
        ]
      }
    ]
  },
  plugins: [
    /**
     * Copies .node module into build folder to prevent webpack bundler from linking with the wrong __dirname path.
     * See: https://github.com/prebuild/node-gyp-build/issues/22
     */
    new CopyPlugin({ patterns: [{ 
      from: 'node_modules/fuse-native/build/Release/fuse.node',
      to: 'build/Release/fuse.node'
    },
    {
      from: './assets',
      to: 'assets'
    },
    {
      from: './src/config.json',
      to: 'config.json'
    }
  ]})
  ],
  resolve: {
    extensions: [".tsx", ".ts", ".js", ".jsx", ".mjs"]
  },
};
