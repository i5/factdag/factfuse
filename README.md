# factFUSE
<img align="left" src="/assets/logoSimple.png" width="250" height="250"> 
<br/>
<br/>

A user-space file system that allows users to interface with LDP servers and mount them to the local file system. 
factFUSE uses [factlib.js](https://git.rwth-aachen.de/i5/factdag/factlibjs) to manage provenance information according to the FactDAG Model and provides UI Extensions to view and manage this information directly from the file system. 
It implements an easy-to-use commit system to synchronize local changes to the LDP while creating and preserving provenance. 

![factFUSE Demo](https://git.rwth-aachen.de/i5/factdag/factfuse/-/wikis/uploads/e02c9781489e8f0d37218010e5d57832/factFUSE_Demo.mp4)

### Usability Study
To evaluate the usability of factFUSE and the adoption of version- and provenance management principles by inexperienced users, we conducted a user study.
If you are interested in the results of the study, visit the [wiki pages](https://git.rwth-aachen.de/i5/factdag/factfuse/-/wikis).

### Installation

#### Pre-Builds
The easiest way to use factFUSE is to download the latest version from the [release](https://git.rwth-aachen.de/i5/factdag/factfuse/-/releases) page.

The macOS version ships as a mountable disk image. Simply mount the volume and follow the instructions:
![dmg_screenshot](/assets/dmg_screenshot.png)

The Linux version comes as a tar archive. It contains an executable AppImage and a .desktop file for the context-menu utilities.
To launch the Linux version, execute the `AppRun` link inside the directory.

#### Building from source
Requires node and npm to be installed. Works on macOS and Linux.

Set up the npm registry for factlib.js 
```
echo @i5:registry=https://git.rwth-aachen.de/api/v4/packages/npm/ >> .npmrc
```
For information on the installation on factlib.js and on how to setup a LDP with factlib.js, see [here](https://git.rwth-aachen.de/i5/factdag/factlibjs).

Install all dependencies for nodegui according to https://github.com/nodegui/nodegui. 

Clone this repository, install the required dependencies and rebuild the fuse-native node native extensions for inclusion in the build.
```
git clone https://git.rwth-aachen.de/i5/factdag/factfuse.git
npm install
npx qode `which npm` install fuse-native --build-from-source
```

Then run and wait for webpack
```
npm run webpack
```

Finally build the application using
```
npm run build
```

You can find the finished build under `~/factfuse/deploy/<darwin or linux>/build/`

#### Using the context-menu utilities
The OS-specific utilities can be found in [/contextmenu](https://git.rwth-aachen.de/i5/factdag/factfuse/-/tree/master/contextmenu).
If you want to use the utilities, please make sure that the specified paths point to your factFUSE installation.

**For macOS:**
The macOS utilities can be installed with Automator.
If factFUSE is not in your Applications folder: Open the Automator Quick Action, change the Shell Script variable `loc` to point to the location of the factFUSE app.

**For Linux:**
Use FileManager-Actions for the Linux utilities. (`sudo apt-get install filemanager-actions`)
Open the .desktop file with FileManager-Actions and select the 'View History' Action. In the 'Command' Tab, change the working directory field to point to your factFUSE directory that contains the AppImage. 

### Running factFUSE from source
The following npm-scripts can be used inside the cloned project folder:

* `npm run webpack` : Run webpack before manually using the project and leave the process running. 
* `npm run init` : Starts factFUSE and opens the configuration GUI.
* `npm run build` : Builds an app from source.

### Getting started - A Tutorial

![Slide1](/assets/tutorial/slides/Slide1.png)
![Slide2](/assets/tutorial/slides/Slide2.png)
![Slide3](/assets/tutorial/slides/Slide3.png)
![Slide4](/assets/tutorial/slides/Slide4.png)
![Slide5](/assets/tutorial/slides/Slide5.png)
![Slide6](/assets/tutorial/slides/Slide6.png)
![Slide7](/assets/tutorial/slides/Slide7.png)
![Slide8](/assets/tutorial/slides/Slide8.png)
![Slide9](/assets/tutorial/slides/Slide9.png)
![Slide10](/assets/tutorial/slides/Slide10.png)
![Slide11](/assets/tutorial/slides/Slide11.png)


