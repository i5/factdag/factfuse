import { QMainWindow, QToolButton,QShortcut,CursorShape,QSystemTrayIcon,QMenu, QAction,QApplication,QIcon,QStatusBar,QPushButton, WidgetEventTypes,QLabel,  QWidget, FlexLayout,QGridLayout,ButtonRole, QBoxLayout,QLineEdit,QTextEdit,QGroupBox, QCheckBox,QFileDialog,FileMode,QListWidget,QListWidgetItem, QMessageBox, QKeySequence } from "@nodegui/nodegui";
const config = require('../config.json')
const fs = require('fs')
const pat = require('path')
import {FactFUSE} from '../FileSystemHandler/fileSystemHandler'
import {LdpMementoClient} from "@i5/factlib.js/index";
import { setTitleBarStyle } from '@nodegui/plugin-title-bar'
import { Info } from "@nodegui/os-utils";
import { CommitGUI } from "./CommitGUI";
const service = new LdpMementoClient();
const idFactory = service.getIDFactory();
import {spawn,exec,execFile,fork} from 'child_process';
var fuseFS:FactFUSE;
//var folderIcon = new QIcon(pat.dirname(__dirname)+"/assets/folder.png")
var folderIcon = new QIcon(pat.join(__dirname,"/assets/folder.png"))
const folderIconLight = new QIcon(pat.join(__dirname,"/assets/folderLight.png"))
const logo = new QIcon(pat.join(__dirname,"/assets/logoSimple.png"))
const logoMounted = new QIcon(pat.join(__dirname,"/assets/logoMounted.png"))
var tutorialOpen = false;
var helpOpen = false;

export class InitGUI {
    win = new QMainWindow();
    rootView = new QWidget();
    rootLayout = new QBoxLayout(2);
    inputArea = new QWidget();
    inputLayout = new QGridLayout();

    addressLabel = new QLabel();
    rootLabel = new QLabel();
    mountLabel = new QLabel();
    userLabel = new QLabel();
    outputLabel = new QLabel();
    autoCommitLabel = new QLabel();

    addressEdit = new QLineEdit();
    rootEdit = new QLineEdit();
    mountEdit = new QLineEdit();
    userEdit = new QLineEdit();
    outputEdit = new QLineEdit();

    mountFolderButton = new QPushButton();
    outputFolderButton = new QPushButton();
    mountButton = new QPushButton();
    autoCommitBox = new QCheckBox();

    tray = new QSystemTrayIcon();
    mountStatusAction = new QAction();
    statusAction = new QAction();
    mountAction = new QAction();
    unmountAction = new QAction();
    autoCommitAction = new QAction();
    commitAction = new QAction();

    helpButton = new QToolButton();
    toolTipBar = new QStatusBar();

    mounted:boolean;

    constructor() {
        const qApp = QApplication.instance();

        if(config.showTutorialonStart) {
          tutorialOpen = true;
          require('./tutorial').openTutorial(function() {
            tutorialOpen = false;
          });
        }
        else {
          this.win.show();
          this.win.raise();
        }

        //const trayIcon = new QIcon(pat.resolve(__dirname, "some/image/file.png"));
        const trayMenu = new QMenu();


        this.statusAction.setIcon(new QIcon(pat.join(__dirname,"/assets/running.png")));
        this.statusAction.setEnabled(false);
        this.statusAction.setText("factFUSE is running")
        trayMenu.addAction(this.statusAction);

        const sep0 = new QAction();
        sep0.setSeparator(true)
        trayMenu.addAction(sep0);

        const openAction = new QAction();
        openAction.setText("Open GUI");
        openAction.addEventListener('triggered',()=> {
          this.win.setWindowState(8)
          this.win.show();
          this.win.raise();
        });
        trayMenu.addAction(openAction);
        this.mountStatusAction.setText("No active mount");
        if(Info.isDarkMode()) {
          this.mountStatusAction.setIcon(folderIconLight);
        }
        else {
          this.mountStatusAction.setIcon(folderIcon);
        }
        this.mountStatusAction.setEnabled(false)
        trayMenu.addAction(this.mountStatusAction);

        const sep = new QAction();
        sep.setSeparator(true)
        trayMenu.addAction(sep);
        this.mountAction.setText("Connect");
        this.mountAction.addEventListener("triggered", () => {
          this.mountButton.click();
        });
        this.unmountAction.setText("Disconnect");
        this.unmountAction.setEnabled(false)
        this.unmountAction.addEventListener("triggered", () => {
          this.mountButton.click();
        });
        trayMenu.addAction(this.mountAction);
        trayMenu.addAction(this.unmountAction);

        const sep3 = new QAction();
        sep3.setSeparator(true)
        trayMenu.addAction(sep3);

        this.autoCommitAction.setText("Auto Sync");
        this.autoCommitAction.setCheckable(true);
        this.autoCommitAction.setChecked(true);
        this.autoCommitAction.addEventListener("changed",()=> {
          this.autoCommitBox.setChecked(this.autoCommitAction.isChecked())
        });
        trayMenu.addAction(this.autoCommitAction);

        this.commitAction.setText("Manual Upload");
        this.commitAction.setEnabled(false);
        this.commitAction.addEventListener("triggered",()=> {
          new CommitGUI();
        });
        trayMenu.addAction(this.commitAction);

        const sep4 = new QAction();
        sep4.setSeparator(true)
        trayMenu.addAction(sep4);

        const helpAction = new QAction();
        helpAction.setText("Help");
        helpAction.addEventListener('triggered',()=> {
          if(!tutorialOpen) {
            tutorialOpen = true;
            require('./tutorial').openTutorial(function() {
              tutorialOpen = false;
            });
          }
          else {
            require('./tutorial').maximizeTutorial();
          }
        });
        trayMenu.addAction(helpAction);
        
        const sep2 = new QAction();
        sep2.setSeparator(true)
        trayMenu.addAction(sep2);

        const quitAction = new QAction();
        quitAction.setShortcut(new QKeySequence(`Ctrl+Q`));
        quitAction.setText("Quit");
        quitAction.addEventListener('triggered',()=> {
          fuseFS.unmount(function() { //wait for the fs to unmount before quitting
            qApp.quit();
          });
        });
        trayMenu.addAction(quitAction);

        const trayIcon = logo;
        if(Info.isDarkMode() || process.platform=="linux") {
          this.tray.setIcon(new QIcon(pat.join(__dirname,"/assets/logoSimpleLight.png")));
        }
        else {
          this.tray.setIcon(trayIcon);
        }
        this.tray.addEventListener('messageClicked',(global as any).openMountpoint);
        this.tray.setContextMenu(trayMenu);
        this.tray.setToolTip("factFUSE")
        this.tray.show();
        
        (global as any).trayStatus = this.statusAction;
        (global as any).trayMenu = trayMenu;
        (global as any).tray = this.tray;

        /**
         * Window
         * 
         * 
         * 
         */
        process.title = "factFUSE"
        this.win.setWindowTitle("factFUSE - Configuration");
        if(process.platform=="linux") {
          this.win.setWindowIcon(new QIcon(pat.join(__dirname,"/assets/logoSimpleLight.png")))
        }
        else {
          this.win.setWindowIcon(new QIcon(pat.join(__dirname,"/assets/logoSimple.png")))
        }
        this.win.addEventListener(WidgetEventTypes.Close, () => {
          if(this.mounted) {
            fuseFS.unmount(function() { //wait for the fs to unmount before quitting
              qApp.quit();
            });
          }
        })
        //Dark mode/light mode change detection
        this.win.addEventListener(WidgetEventTypes.PaletteChange, () => {
          if(Info.isDarkMode()) {
            this.mountFolderButton.setIcon(folderIconLight)
            this.outputFolderButton.setIcon(folderIconLight)
            this.mountStatusAction.setIcon(folderIconLight);
            if(this.mounted) {
              this.mountButton.setIcon(new QIcon());
              if((global as any).systemStatus == 1) {
                this.tray.setIcon(new QIcon(pat.join(__dirname,"/assets/logoMountedLight.png")));
              }
              else if((global as any).systemStatus == 0) {
                this.tray.setIcon(new QIcon(pat.join(__dirname,"/assets/logoWorkingLight.png")));
              }
              else if((global as any).systemStatus == -1) {
                this.tray.setIcon(new QIcon(pat.join(__dirname,"/assets/logoErrorLight.png")));
              }
            }
            else {
              this.mountButton.setIcon(new QIcon(pat.join(__dirname,"assets/connectLight.png")));
              this.tray.setIcon(new QIcon(pat.join(__dirname,"/assets/logoSimpleLight.png")));
            }
          }
          else if(process.platform=="linux") {
            if(this.mounted) {
              this.mountButton.setIcon(new QIcon());
              if((global as any).systemStatus == 1) {
                this.tray.setIcon(new QIcon(pat.join(__dirname,"/assets/logoMountedLight.png")));
              }
              else if((global as any).systemStatus == 0) {
                this.tray.setIcon(new QIcon(pat.join(__dirname,"/assets/logoWorkingLight.png")));
              }
              else if((global as any).systemStatus == -1) {
                this.tray.setIcon(new QIcon(pat.join(__dirname,"/assets/logoErrorLight.png")));
              }
            }
            else {
              this.tray.setIcon(new QIcon(pat.join(__dirname,"/assets/logoSimpleLight.png")));
            }
          }
          else {
            this.mountFolderButton.setIcon(folderIcon)
            this.outputFolderButton.setIcon(folderIcon)
            this.mountStatusAction.setIcon(folderIcon);
            if(this.mounted) {
              this.mountButton.setIcon(new QIcon());
              if((global as any).systemStatus == 1) {
                this.tray.setIcon(new QIcon(pat.join(__dirname,"/assets/logoMounted.png")));
              }
              else if((global as any).systemStatus == 0) {
                this.tray.setIcon(new QIcon(pat.join(__dirname,"/assets/logoWorking.png")));
              }
              else if((global as any).systemStatus == -1) {
                this.tray.setIcon(new QIcon(pat.join(__dirname,"/assets/logoError.png")));
              }
            }
            else {
              this.mountButton.setIcon(new QIcon(pat.join(__dirname,"assets/connect.png")));
              this.tray.setIcon(new QIcon(pat.join(__dirname,"/assets/logoSimple.png")));
            }
          }
        })

        this.mounted = false;

        this.rootView.setObjectName("rootView");
        this.rootView.setLayout(this.rootLayout);

        this.win.setStatusBar(this.toolTipBar);
        this.toolTipBar.hide();
        this.toolTipBar.showMessage("Hover over items for help")
        
        this.rootLayout.addWidget(this.helpButton);
        this.helpButton.setText("?");
        this.helpButton.addEventListener('clicked',()=> {
            if(!helpOpen) {
              helpOpen = true;
              openHelpWindow(function() {
                helpOpen = false;
              });
            }
        });

        this.rootLayout.addWidget(this.inputArea);
        this.inputArea.setLayout(this.inputLayout);

        this.addressLabel.setBuddy(this.addressEdit)
        //this.addressLabel.setCursor(CursorShape.PointingHandCursor)
        this.addressLabel.addEventListener(WidgetEventTypes.ToolTip, () => {
          this.toolTipBar.showMessage("LDP Server: The server you want to connect to")
        })
        this.addressLabel.setText("LDP Server: ")
        this.addressEdit.setText(config.ldpAddress)
        this.inputLayout.addWidget(this.addressLabel,0,0)
        this.inputLayout.addWidget(this.addressEdit,0,1)

        this.userLabel.setBuddy(this.userEdit)
        this.userLabel.addEventListener(WidgetEventTypes.ToolTip, () => {
          this.toolTipBar.showMessage("User ID: Your username")
        })
        this.userLabel.setText("User ID: ")
        this.userEdit.setText(config.userID)
        this.inputLayout.addWidget(this.userLabel,1,0)
        this.inputLayout.addWidget(this.userEdit,1,1)

        this.mountLabel.setBuddy(this.mountEdit)
        this.mountLabel.addEventListener(WidgetEventTypes.ToolTip, () => {
          this.toolTipBar.showMessage("Target: The folder where the files will be displayed")
        })
        this.mountLabel.setText("Target: ")
        this.mountEdit.setText(config.mountpoint)
        this.inputLayout.addWidget(this.mountLabel,2,0)
        this.inputLayout.addWidget(this.mountEdit,2,1)
        if(Info.isDarkMode()) {
          this.mountFolderButton.setIcon(folderIconLight);
        }
        else {
          this.mountFolderButton.setIcon(folderIcon);
        }
        this.mountFolderButton.addEventListener('clicked',()=> {
          const forked = fork(pat.join(__dirname,'dialogOpener.js'),['Choose a mountpoint',pat.dirname(this.mountEdit.text()),2])
          forked.on('message', (m:any) => {
            this.mountEdit.setText(m[0])
            forked.kill();
          });
        });
        this.inputLayout.addWidget(this.mountFolderButton,2,2)

        this.outputLabel.setBuddy(this.outputEdit)
        this.outputLabel.addEventListener(WidgetEventTypes.ToolTip, () => {
          this.toolTipBar.showMessage("Download: The folder where downloaded files be saved")
        })
        this.outputLabel.setText("Download: ")
        this.outputEdit.setText(config.output)
        this.outputEdit.setReadOnly(true);
        this.inputLayout.addWidget(this.outputLabel,3,0)
        this.inputLayout.addWidget(this.outputEdit,3,1)
        if(Info.isDarkMode()) {
          this.outputFolderButton.setIcon(folderIconLight);
        }
        else {
          this.outputFolderButton.setIcon(folderIcon);
        }
        this.outputFolderButton.addEventListener('clicked',()=> {
          const forked = fork(pat.join(__dirname,'dialogOpener.js'),['Choose a folder',pat.dirname(this.outputEdit.text()),2])
          forked.on('message', (m:any) => {
            this.outputEdit.setText(m[0])
            forked.kill();
          });
        });
        this.inputLayout.addWidget(this.outputFolderButton,3,2)

        this.inputLayout.addWidget(this.autoCommitLabel,4,0);
        this.autoCommitLabel.setText("Auto Sync: ");
        this.autoCommitLabel.addEventListener(WidgetEventTypes.ToolTip, () => {
          this.toolTipBar.showMessage("Auto Sync: Select to automatically upload your changes")
        })
        this.inputLayout.addWidget(this.autoCommitBox,4,1);
        this.autoCommitBox.setChecked(true);
        this.autoCommitBox.addEventListener('stateChanged',()=> {
          if(this.autoCommitBox.checkState()) {
            this.commitAction.setEnabled(false)
            this.autoCommitAction.setChecked(true);
            //this.autoCommitAction.setText("Auto Sync")
            config.autoCommitEnabled = true;
            if(fuseFS) {
              fuseFS.startAutoCommitter();
            }
          }
          else {
            this.commitAction.setEnabled(true)
            this.autoCommitAction.setChecked(false);
            //this.autoCommitAction.setText("Enable Auto Sync")
            config.autoCommitEnabled = false;
            if(fuseFS) {
              fuseFS.stopAutoCommitter();
            }
            this.statusAction.setIcon(new QIcon(pat.join(__dirname,"/assets/running.png")));
            this.statusAction.setText("factFUSE is running")
          }
          //fs.writeFileSync('./src/config.json', JSON.stringify(config, null, 2));
          fs.writeFileSync(pat.join(__dirname, '/config.json'), JSON.stringify(config, null, 2));
        });


        this.rootLayout.addWidget(this.mountButton);
        this.mountButton.setObjectName("mountButton");
        this.mountButton.setText("Connect")
        if(Info.isDarkMode()) {
          this.mountButton.setIcon(new QIcon(pat.join(__dirname,"/assets/connectLight.png")))
        }
        else {
          this.mountButton.setIcon(new QIcon(pat.join(__dirname,"/assets/connect.png")))
        }
        this.mountButton.addEventListener('clicked',()=> {
          this.mountSwitch();
        });

        this.win.setFixedSize(450,300)
        this.win.setCentralWidget(this.rootView);
        (global as any).initWindow = this.win;
    }

    async mountSwitch() {
      const qApp = QApplication.instance();
      if(!this.mounted) {
          this.mounted = true;
          config.ldpAddress = this.addressEdit.text();
          config.mountpoint = this.mountEdit.text();
          config.userID = this.userEdit.text();
          config.output = this.outputEdit.text();
          if(this.autoCommitBox.checkState()) {
            config.autoCommitEnabled = true;
          }
          else {
            config.autoCommitEnabled = false;
          }
          //fs.writeFileSync('./src/config.json', JSON.stringify(config, null, 2));
          fs.writeFileSync(pat.join(__dirname, '/config.json'), JSON.stringify(config, null, 2));
          
          try {
            (global as any).systemStatus = 1;
            await this.testConnection(this.addressEdit.text());
            fs.mkdirSync(this.mountEdit.text(), { recursive: true });
            var self = this;
            setTimeout(function() {
              fuseFS = new FactFUSE(config.mountpoint);
              self.win.setWindowState(1)
              self.activateMountedState();
              qApp.setQuitOnLastWindowClosed(false);
              fuseFS.mount(); // only works when modifying the path in node-gyp-builder. webpack seems to send '/' to gyp instead of .../fuse-native. 
              if(process.platform == "linux") {
                self.tray.showMessage("Success!","LDP mounted on " + config.mountpoint,undefined,3000);
              }
              else {
                self.tray.showMessage("Success!","LDP mounted on " + config.mountpoint,folderIcon,3000);
              }
              if(Info.isDarkMode() || process.platform=="linux") {
                self.tray.setIcon(new QIcon(pat.join(__dirname,"/assets/logoMountedLight.png")))
              }
              else {
                self.tray.setIcon(logoMounted);
              }
              self.mountStatusAction.setEnabled(true)
              self.mountStatusAction.addEventListener("triggered",(global as any).openMountpoint);
              self.mountStatusAction.setText(pat.basename(config.mountpoint) + ": " + config.ldpAddress);
              self.mountAction.setEnabled(false)
              self.unmountAction.setEnabled(true)
            },500);
          }
          catch(e) {
            this.mounted = false;
            this.tray.showMessage("Mount failed",e+"",new QIcon(pat.join(__dirname,"/assets/error.png")),3000);
            const messageBox = new QMessageBox();
            messageBox.setText('Mount failed')
            messageBox.setInformativeText('There was an error while trying to mount the LDP.');
            messageBox.setDetailedText(e.toString())
            const accept = new QPushButton();
            accept.setText('Ok');
            messageBox.addButton(accept, ButtonRole.AcceptRole);
            messageBox.exec();
          }
      }
      else {
          this.mounted = false;
          this.activateUnmountedState();
          qApp.setQuitOnLastWindowClosed(true);
          fuseFS.unmount(function(){});
          if(process.platform == "linux") {
            this.tray.showMessage("Unmounted!","LDP unmounted.",undefined,3000);          }
          else {
            this.tray.showMessage("Unmounted!","LDP unmounted.",folderIcon,3000);          }
          if(Info.isDarkMode() || process.platform=="linux") {
            this.tray.setIcon(new QIcon(pat.join(__dirname,"/assets/logoSimpleLight.png")));
          }
          else {
            this.tray.setIcon(logo);
          }
          this.mountStatusAction.setEnabled(false)
          this.mountStatusAction.setText("No active mount");
          this.mountAction.setEnabled(true)
          this.unmountAction.setEnabled(false)
      }
    }

    activateMountedState() {
        this.addressEdit.setEnabled(false);
        this.addressLabel.setEnabled(false);
        this.rootEdit.setEnabled(false);
        this.rootLabel.setEnabled(false);
        this.mountEdit.setEnabled(false);
        this.mountLabel.setEnabled(false);
        this.userEdit.setEnabled(false);
        this.userLabel.setEnabled(false);
        this.outputEdit.setEnabled(false);
        this.outputLabel.setEnabled(false);
        this.mountFolderButton.setEnabled(false);
        this.outputFolderButton.setEnabled(false);
        this.mountButton.setText("Disconnect");
        this.mountButton.setIcon(new QIcon())
        this.mountButton.setStyleSheet(`
        #mountButton {
          color: red;
        }
      `);
    }

    activateUnmountedState() {
        this.addressEdit.setEnabled(true);
        this.addressLabel.setEnabled(true);
        this.rootEdit.setEnabled(true);
        this.rootLabel.setEnabled(true);
        this.mountEdit.setEnabled(true);
        this.mountLabel.setEnabled(true);
        this.userEdit.setEnabled(true);
        this.userLabel.setEnabled(true);
        this.outputEdit.setEnabled(true);
        this.outputLabel.setEnabled(true);
        this.mountFolderButton.setEnabled(true);
        this.outputFolderButton.setEnabled(true);
        this.mountButton.setText("Connect");
        if(Info.isDarkMode()) {
          this.mountButton.setIcon(new QIcon(pat.join(__dirname,"/assets/connectLight.png")))
        }
        else {
          this.mountButton.setIcon(new QIcon(pat.join(__dirname,"/assets/connect.png")))
        }
        this.mountButton.setStyleSheet(`
        @media (prefers-color-scheme: dark) { 
          #mountButton {
            color: white;
          }
        }
        @media (prefers-color-scheme: light) { 
          #mountButton {
            color: black;
          }
        }
      `);
    }

    async testConnection(address:string) {
      const rID = idFactory.parseURIToResourceID(address);
      await service.getDirectory(rID);
    }
}

(global as any).openMountpoint = function () {
  if(process.platform=="linux") {
    //exec('nautilus ' + config.mountpoint)
    exec('xdg-open ' + config.mountpoint)
    //exec('xdg-open %s', config.mountpoint)
  }
  else {
    exec('open '+config.mountpoint);
  }
}

function openHelpWindow(cb:any) {
  const helpWindow = new QMainWindow();
  helpWindow.setWindowTitle("Help");
  const helpView = new QWidget();
  const helpLayout = new QGridLayout();
  helpLayout.setVerticalSpacing(25)
  helpView.setLayout(helpLayout)

  const title = new QLabel();
  title.setText("\n LDP Server:   ")
  title.setInlineStyle("font-weight: bold;")
  const helpTitle = new QLabel();
  helpTitle.setText("\n The Server you want to connect to.")
  title.setBuddy(helpTitle)

  const changesLabel = new QLabel();
  changesLabel.setText("User ID:   ")
  changesLabel.setInlineStyle("font-weight: bold;");
  const helpChanges = new QLabel();
  helpChanges.setText("Your username.")

  const usedLabel = new QLabel();
  usedLabel.setText("Target:   ")
  usedLabel.setInlineStyle("font-weight: bold;");
  const helpUsed = new QLabel();
  helpUsed.setText("The folder where the files will be displayed. The server will be mounted here.")

  const commentLabel = new QLabel();
  commentLabel.setText("Download:   ")
  commentLabel.setInlineStyle("font-weight: bold;");
  const helpComment = new QLabel();
  helpComment.setText("The folder where downloaded files be saved.")

  const syncLabel = new QLabel();
  syncLabel.setText("Auto Sync:   ")
  syncLabel.setInlineStyle("font-weight: bold;");
  const helpSync = new QLabel();
  helpSync.setText("Select this to automatically upload changes to the server.")

  helpLayout.addWidget(helpTitle,0,1)
  helpLayout.addWidget(title,0,0)
  helpLayout.addWidget(helpChanges,1,1)
  helpLayout.addWidget(changesLabel,1,0)
  helpLayout.addWidget(helpUsed,2,1)
  helpLayout.addWidget(usedLabel,2,0)
  helpLayout.addWidget(helpComment,3,1)
  helpLayout.addWidget(commentLabel,3,0)
  helpLayout.addWidget(syncLabel,4,0)
  helpLayout.addWidget(helpSync,4,1)

  helpWindow.setCentralWidget(helpView)
  helpWindow.show();
  helpWindow.addEventListener(WidgetEventTypes.Close, () => {
    cb();
  });
  setTitleBarStyle(helpWindow.native, 'hidden'); // or hiddenInset
  (global as any).conifgHelpWindow = helpWindow;
}