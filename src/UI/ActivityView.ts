import {LdpMementoClient} from "@i5/factlib.js/index";
import {Namespaces} from "@i5/factlib-utils"
import {RevisionView} from "./RevisionView"
import {downloadResource} from "./provView";
import { Info } from "@nodegui/os-utils";
const service = new LdpMementoClient();
const idFactory = service.getIDFactory();
const pat = require("path");
const rdf = require("rdflib");
const mime = require('mime-types')
const config = require('../config.json')
import {openWindows} from './provView'

//-------------------------------------------------------------------------------

const { QMainWindow,QTreeWidgetItem, WidgetEventTypes,QIcon, QTreeWidget, QPushButton, QToolButton,QLabel, QWidget,QGridLayout, QBoxLayout } = require("@nodegui/nodegui");

const downloadIcon = new QIcon(pat.join(__dirname,"/assets/download.png"))

export class ActivityView {
    uri: string;
    height = 100;
    win = new QMainWindow();
    rootView = new QWidget();
    rootLayout = new QBoxLayout(2);
    topField = new QWidget();
    topLayout = new QGridLayout();

    usedTree = new QTreeWidget();
    authLabel = new QLabel();
    timeLabel = new QLabel();
    usedLabel = new QLabel();
    commentLabel = new QLabel();

    entButtons: typeof QPushButton = [];
    actDownButtons: typeof QPushButton = [];
    usedItems:any = []

    constructor(uri:string) {
        this.uri = uri;
        this.createWindow(this.uri);
    }

    async createWindow(uri:string) {
        const resourceID = idFactory.parseURIToResourceID(uri)
        this.win.setWindowTitle(resourceID.resourceName + " | Activity View");
        this.win.setObjectName(uri);
        this.win.addEventListener(WidgetEventTypes.Close, () => {
            openWindows.splice(openWindows.map((x:any)=>x.objectName()).indexOf(uri),1)
        });
        this.win.addEventListener(WidgetEventTypes.PaletteChange, () => {
            if(Info.isDarkMode()) {
                this.usedTree.setStyleSheet(`QTreeWidget::item:!selected { border-bottom: 1px solid gray;}`);
                for(const button of this.actDownButtons) {
                    button.setIcon(new QIcon(pat.join(__dirname,"/assets/downloadLight.png")))
                }
            }
            else {
                this.usedTree.setStyleSheet(`QTreeWidget::item:!selected { border-bottom: 1px solid black;}`);
                for(const button of this.actDownButtons) {
                    button.setIcon(new QIcon(pat.join(__dirname,"/assets/download.png")))
                }
            }
        });

        this.rootView.setObjectName("rootView");
        this.rootView.setLayout(this.rootLayout);
        this.topField.setLayout(this.topLayout);

        this.rootLayout.addWidget(this.topField);
        this.rootLayout.addWidget(this.usedLabel);
        this.rootLayout.addWidget(this.usedTree);

        this.topLayout.addWidget(this.authLabel,0,0);
        this.topLayout.addWidget(this.timeLabel,1,0);
        this.topLayout.addWidget(this.commentLabel,2,0);
    
        this.usedLabel.setText(" Used Resources: ");

        this.usedTree.setColumnCount(5)
        this.usedTree.setHeaderLabels(["Name", "Server", "Path", "RevisionID", "Download"])
        this.usedTree.setColumnWidth(0,125)
        this.usedTree.setColumnWidth(1,150)
        this.usedTree.setColumnWidth(2,150)
        this.usedTree.setColumnWidth(3,200)
        if(Info.isDarkMode()) {
            this.usedTree.setStyleSheet(`QTreeWidget::item:!selected { border-bottom: 1px solid gray;}`);
        }
        else {
            this.usedTree.setStyleSheet(`QTreeWidget::item:!selected { border-bottom: 1px solid black;}`);
        }

        this.win.resize(755,300)
        this.win.setCentralWidget(this.rootView);
        this.win.show();
        openWindows.push(this.win);
        (global as any).actWin = this.win;

        this.addUsedEntities(uri)
    }

    async addUsedEntities(uri:string) {
        try { //try to recieve specific activity if the uri is suitable for a factID, else get the last version
            const factID = idFactory.parseURIToFactID(uri)
            var activity = await service.getSpecificActivityRevision(factID)
        }
        catch {
            const resourceID = idFactory.parseURIToResourceID(uri)
            var activity = await service.getLastActivityRevision(resourceID)
        }
        if(activity.queryStore(Namespaces.PROV(Namespaces.PROV_RELATION.WAS_ASSOCIATED_WITH))[0]){
            const processName = pat.basename(activity.queryStore(Namespaces.PROV(Namespaces.PROV_RELATION.WAS_ASSOCIATED_WITH))[0].object.value)
            this.authLabel.setText("Author:   " + processName);
        }
        //this.timeLabel.setText("Time:   " + new Date(activity.factID.revisionID).toLocaleString('de-De', { timeZone: 'UTC' }))
        this.timeLabel.setText("Time:   " + new Date(activity.factID.revisionID).toLocaleString())
        const rdfs = new rdf.Namespace("http://www.w3.org/2000/01/rdf-schema#");
        try {
            this.commentLabel.setText("\nComment:   " + activity.queryStore(rdfs("comment"))[0].object.value)
        }
        catch {
            this.commentLabel.setText("\nComment:   ");
        }
        const used = activity.queryStore(Namespaces.PROV(Namespaces.PROV_RELATION.USED))
        var j = 0;
        for(const element of used) {
            const usedEntity = element.object.value.toString()
            const entityID = idFactory.parseURIToFactID(usedEntity)
            const fact = await service.getSpecificFact(entityID)

            if(await service.isResourceBinary(entityID)){
                var extension = '.'+mime.extension((await service.getLastBinaryFactRevision(entityID,"")).contentType);
            }
            else {
                extension = config.rdfExtension;
            }

            this.usedItems[j] = new QTreeWidgetItem(this.usedTree, ["", fact.authorityResourceID.authorityID, fact.resourceID.resourceID+extension, entityID.revisionID])

            this.entButtons[j] = new QPushButton();
            this.entButtons[j].setObjectName("ent"+j)
            this.entButtons[j].setText(entityID.resourceName)
            this.entButtons[j].addEventListener('clicked',()=>{
                if(!openWindows.map((x:any)=>x.objectName()).includes(fact.resourceID.resourceID)) {
                    new RevisionView(fact.resourceID.resourceURI.toString(),entityID.revisionID)
                }
                else {
                    const index = openWindows.map((x:any)=>x.objectName()).indexOf(fact.resourceID.resourceID);
                    openWindows[index].setWindowState(8);
                    openWindows[index].raise();
                }
            });
            (global as any)['ent'+j] = this.entButtons[j]; // hopefully prevents buttons from being garbage collected
            this.usedTree.setItemWidget(this.usedItems[j],0,this.entButtons[j])

            //add download button to each revision
            this.actDownButtons[j] = new QToolButton();
            this.actDownButtons[j].setObjectName("downUsed"+j)
            this.actDownButtons[j].setText('Download')
            if(Info.isDarkMode()) {
                this.actDownButtons[j].setIcon(new QIcon(pat.join(__dirname,"/assets/downloadLight.png")))
            }
            else {
                this.actDownButtons[j].setIcon(downloadIcon)
            }
            this.actDownButtons[j].setInlineStyle("border : none;")
            this.actDownButtons[j].addEventListener('clicked',()=> downloadResource(fact));
            this.usedTree.setItemWidget(this.usedItems[j],4,this.actDownButtons[j])
            j++;
        }
    }
}