import {LdpMementoClient, Fact} from "@i5/factlib.js/index";
import {ActivityView} from "./ActivityView"
import {downloadResource} from "./provView"
import {Namespaces} from "@i5/factlib-utils"
import {Stream} from "stream";
const service = new LdpMementoClient();
const pat = require('path')
const mime = require('mime-types')
const config = require('../config.json')
const idFactory = service.getIDFactory();
const RevisionRestorer = require('../LocalChangeManager/restoreRevision/restoreRevision')
const dLogoPath = pat.join(__dirname,"/assets/download.png");
const rLogoPath = pat.join(__dirname,"/assets/restore.png");
import {openWindows} from './provView'
import { Info } from "@nodegui/os-utils";

//-------------------------------------------------------------------------------

const { QMainWindow,QApplication, QTabWidget, WidgetEventTypes,QTreeWidgetItem,QIcon, QLabel, QPixmap,QTreeWidget, QPushButton, QToolButton, QMessageBox, ButtonRole } = require("@nodegui/nodegui");

//const downloadIcon = new QIcon("/Users/lmueller/GitLab/factfuse/assets/download.png")
const downloadIcon = new QIcon(dLogoPath)
const restoreIcon = new QIcon(rLogoPath)

export class RevisionView {
    uri: string;
    highlightedRevision: string;
    height = 100;
    win = new QMainWindow();
    tree = new QTreeWidget();
    tabWidget = new QTabWidget();
    actButtons: typeof QPushButton = []
    downloadButtons: typeof QPushButton = [];
    restoreButtons: typeof QPushButton = [];

    constructor(uri:string,revisionID="") {
        this.uri = uri;
        this.highlightedRevision = revisionID;
        this.checkIfExists();
    }

    async checkIfExists() {
        const resourceID = idFactory.parseURIToResourceID(this.uri)
        const exists = await service.isResourceExistent(resourceID);
        if(!exists) {
            const messageBox = new QMessageBox();
            messageBox.setText('Resource does not exist anymore.')
            messageBox.setInformativeText('Resource has been deleted. \n You can download this revision to access it.');
            const accept = new QPushButton();
            accept.setText('Ok');
            messageBox.addButton(accept, ButtonRole.AcceptRole);
            messageBox.exec();
        }
        else {
            this.createWindow(this.uri);   
        }
    }

    async createWindow(uri:string) {
        const resourceID = idFactory.parseURIToResourceID(uri)

        this.win.setWindowTitle(resourceID.resourceID + " | History");
        this.win.setObjectName(resourceID.resourceID)
        this.win.addEventListener(WidgetEventTypes.Close, () => {
            openWindows.splice(openWindows.map((x:any)=>x.objectName()).indexOf(resourceID.resourceID),1)
        });
        this.win.addEventListener(WidgetEventTypes.PaletteChange, () => {
            if(Info.isDarkMode()) {
                this.tree.setStyleSheet(`QTreeWidget::item:!selected { border-bottom: 1px solid gray;}
                QTreeWidget::item:last { color:green;}`);
            }
            else {
                this.tree.setStyleSheet(`QTreeWidget::item:!selected { border-bottom: 1px solid black;}
                QTreeWidget::item:last { color:green;}`);
            }
        });

        this.tree.setColumnCount(7)
        this.tree.setHeaderLabels(["#","Time", "Server", "Path", "Generating Activity", "Download", "Restore"])
    
        this.tree.setColumnWidth(0,50)
        this.tree.setColumnWidth(1,190)
        this.tree.setColumnWidth(2,150)
        this.tree.setColumnWidth(3,150)
        this.tree.setColumnWidth(4,175)
        this.tree.setColumnWidth(5,75)
        this.tree.setColumnWidth(6,75)
        if(Info.isDarkMode()) {
            this.tree.setStyleSheet(`QTreeWidget::item:!selected { border-bottom: 1px solid gray;}
            QTreeWidget::item:last { color:green;}`);
        }
        else {
            this.tree.setStyleSheet(`QTreeWidget::item:!selected { border-bottom: 1px solid black;}
            QTreeWidget::item:last { color:green;}`);
        }

        this.win.setCentralWidget(this.tree);
        this.win.resize(875,250)
        this.win.addEventListener(WidgetEventTypes.PaletteChange, () => {
          if(Info.isDarkMode()) {
            for(const button of this.downloadButtons) {
                button.setIcon(new QIcon(pat.join(__dirname,"/assets/downloadLight.png")));
            }
            for(const button of this.restoreButtons) {
                button.setIcon(new QIcon(pat.join(__dirname,"/assets/restoreLight.png")));
            }
          }
          else {
            for(const button of this.downloadButtons) {
                button.setIcon(new QIcon(pat.join(__dirname,"/assets/download.png")));
            }
            for(const button of this.restoreButtons) {
                button.setIcon(new QIcon(pat.join(__dirname,"/assets/restore.png")));
            }
          }
        });
        this.win.show();
        (global as any).revGUI = this.win;
        openWindows.push(this.win)

        this.addRevisionItems(uri)
    }

    async addRevisionItems(uri:string) {
        const resourceID = idFactory.parseURIToResourceID(uri)
        const revisionList = await service.getRevisionListByID(resourceID);
        revisionList.reverse()
        var i = 0
        const treeItems = []
        if(await service.isResourceBinary(resourceID)){
            var extension = '.'+mime.extension((await service.getLastBinaryFactRevision(resourceID,"")).contentType);
            var isBinary = true;
        }
        else {
            extension = config.rdfExtension;
            isBinary = false;
        }

        var nextOlderRevision;
        for(const element of revisionList) {
            const fact = await service.getSpecificFact(element.factID)
            const dummyactivityID = idFactory.parseURIToFactID(fact.getGeneratingActivityURI())
            const activityID = idFactory.createFactID(fact.authorityResourceID.authorityID,dummyactivityID.resourceID,dummyactivityID.revisionID)
            
            //add the revisions
            treeItems[i] = new QTreeWidgetItem(this.tree, [(i+1).toString()+".",new Date(element.factID.revisionID).toLocaleString(), element.factID.authorityID, element.factID.resourceID+extension])
            if(this.highlightedRevision == element.factID.revisionID) {
                treeItems[i].setSelected(true);
            }
            
            // add prov:derivation relations as node children
            /*const used = (await service.getSpecificActivityRevision(activityID)).queryStore(Namespaces.PROV(Namespaces.PROV_RELATION.USED))
            if(used.length > 0) {
                new QTreeWidgetItem(treeItems[i], ["","Derived from:"])
                for(const element of used) {
                    const usedEntity = element.object.value.toString()
                    const entityID = idFactory.parseURIToFactID(usedEntity)
                    new QTreeWidgetItem(treeItems[i], ["",entityID.revisionID, entityID.resourceName, entityID.resourceID])
                }
            }*/

            //Add RDF preview
            const previewTreeWidgetItem = new QTreeWidgetItem(treeItems[i], ["",fact.serialize()])
            //Determine differences to last revision 
            if(nextOlderRevision) {
                require('colors');
                const Diff = require('diff');

                const one = nextOlderRevision.serialize();
                const other = fact.serialize();

                const diff = Diff.diffLines(one, other);
                
                diff.forEach((part:any) => {
                const color = part.added ? 'green' :
                    part.removed ? 'red' : 'grey';
                //process.stderr.write(part.value[color]);
                });
                //console.log(diff[0].value['red'])
            }

            //Add binary preview as tree-node children
            if(isBinary) {
                const binaryFact = await service.getSpecificBinaryFact(fact.factID)
                const binaryStreamToBuffer = async (cb:any) =>{
                    const buf:any = [];
                    if (binaryFact.binaryContent instanceof Stream) {
                        binaryFact.binaryContent.on("data", function (chunk:any) {
                        buf.push(chunk)
                      });
                      binaryFact.binaryContent.on("end", function () {
                        cb(Buffer.concat(buf));
                      });
                    }
                    else {
                      cb(Buffer.from(binaryFact.binaryContent));
                    }
                }
                const binaryPreviewLabel = new QLabel();
                await binaryStreamToBuffer(function(buffer:Buffer){
                    const previewPixmap = new QPixmap();
                    previewPixmap.loadFromData(buffer);
                    binaryPreviewLabel.setPixmap(previewPixmap.scaled(150,150,1,1))
                    binaryPreviewLabel.setScaledContents(false)    
                });
                this.tree.setItemWidget(previewTreeWidgetItem,2,binaryPreviewLabel)
            }

            //Add Generating activity buttons
            this.actButtons[i] = new QPushButton();
            this.actButtons[i].setObjectName("act"+i)
            this.actButtons[i].setText(activityID.resourceName)
            this.actButtons[i].addEventListener('clicked',()=>{
                if(!openWindows.map((x:any)=>x.objectName()).includes(activityID.toString())) {
                    new ActivityView(activityID.toString());
                }
                else { //if activity view is already open, raise it
                    const index = openWindows.map((x:any)=>x.objectName()).indexOf(activityID.toString());
                    openWindows[index].setWindowState(8);
                    openWindows[index].raise();
                }
            });
            //this.actButtons[i].addEventListener('clicked',()=>this.tabWidget.addTab(new ActivityView(activityID.toString()).rootView,new QIcon(),resourceID.resourceID));
            this.tree.setItemWidget(treeItems[i],4,this.actButtons[i])

            //add download button to each revision
            this.downloadButtons[i] = new QToolButton();
            this.downloadButtons[i].setObjectName("down"+i)
            this.downloadButtons[i].setText('Download')
            if(Info.isDarkMode()) {
                this.downloadButtons[i].setIcon(new QIcon(pat.join(__dirname,"/assets/downloadLight.png")));
            }
            else {
                this.downloadButtons[i].setIcon(downloadIcon)
            }
            this.downloadButtons[i].setInlineStyle("border : none;")
            this.downloadButtons[i].addEventListener('clicked',()=> downloadResource(fact));
            this.tree.setItemWidget(treeItems[i],5,this.downloadButtons[i])

            //add a restore button for revisions
            if(i==0) {
                treeItems[i].setText(6,"Current")
                this.restoreButtons[i] = new QToolButton;
            }
            else {
                this.restoreButtons[i] = new QToolButton();
                this.restoreButtons[i].setObjectName("restore"+i)
                this.restoreButtons[i].setText(' Restore')
                if(Info.isDarkMode()) {
                    this.restoreButtons[i].setIcon(new QIcon(pat.join(__dirname,"/assets/restoreLight.png")));
                }
                else {
                    this.restoreButtons[i].setIcon(restoreIcon)
                }
                this.restoreButtons[i].setToolButtonStyle(2)
                this.restoreButtons[i].addEventListener('clicked',()=> this.updateWindow(element));
                this.restoreButtons[i].setInlineStyle("border: none;")
                this.tree.setItemWidget(treeItems[i],6,this.restoreButtons[i])
            }
            i++;
            //check if element has older revisions, else break. getRivisionListByID() sometimes returns artifacts
            if(!fact.queryStore(Namespaces.PROV(Namespaces.PROV_RELATION.WAS_REVISION_OF))[0]) {
                break
            }
            nextOlderRevision = fact; //store fact for next revisions diff
        }
        
    }

    async updateWindow(element:any, ) { //used to update the history. mainly after restoring
        const self =this;
        self.tree.clear();
        await RevisionRestorer.restoreRevision(element.factID).then(function() {
            //self.tree.clear();
            self.addRevisionItems(self.uri);
        })
    }

}

//-------------------------------------------------------------------------------



//-------------------------------------------------------------------------------

