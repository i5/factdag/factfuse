import {QPixmap,QIcon,QMainWindow,QWidget,WidgetEventTypes,QBoxLayout, QLabel, QPushButton, QScrollArea, QPicture, QToolButton, QCheckBox} from "@nodegui/nodegui";
const pat = require('path')
const fs = require('fs')
const slidePaths = [pat.join(__dirname,"/assets/tutorial/slides/Slide1.png"),
                    pat.join(__dirname,"/assets/tutorial/slides/Slide2.png"),
                    pat.join(__dirname,"/assets/tutorial/slides/Slide3.png"),
                    pat.join(__dirname,"/assets/tutorial/slides/Slide4.png"),
                    pat.join(__dirname,"/assets/tutorial/slides/Slide5.png"),
                    pat.join(__dirname,"/assets/tutorial/slides/Slide6.png"),
                    pat.join(__dirname,"/assets/tutorial/slides/Slide7.png"),
                    pat.join(__dirname,"/assets/tutorial/slides/Slide8.png"),
                    pat.join(__dirname,"/assets/tutorial/slides/Slide9.png"),
                    pat.join(__dirname,"/assets/tutorial/slides/Slide10.png"),
                    pat.join(__dirname,"/assets/tutorial/slides/Slide11.png")]


const win = new QMainWindow();
export function openTutorial(cb:any) {
    //const win = new QMainWindow();
    const rootView = new QWidget();
    const rootLayout = new QBoxLayout(2);
    const controlField = new QWidget();
    const controlLayout = new QBoxLayout(1);
    const imageLabel = new QLabel();
    const closeButton = new QPushButton()
    const dontshowCheckBox = new QCheckBox();
    const nextButton = new QPushButton();
    const backButton = new QPushButton();
    var currentSlide = 0;
    
    if(process.platform=="linux") {
        win.setWindowIcon(new QIcon(pat.join(__dirname,"/assets/logoSimpleLight.png")))
      }
      else {
        win.setWindowIcon(new QIcon(pat.join(__dirname,"/assets/logoSimple.png")))
      }
    win.setWindowTitle("Tutorial Slide " + (currentSlide+1) + "/" + slidePaths.length)
    imageLabel.setPixmap(new QPixmap(slidePaths[currentSlide]).scaled(1000,800,1,1));
    imageLabel.setScaledContents(true)

    closeButton.setText("Close")
    closeButton.setEnabled(false)
    closeButton.addEventListener('clicked',()=>{
        cb();
        (global as any).initWindow.show();
        (global as any).initWindow.raise();
        win.close();
        
    })

    dontshowCheckBox.setText("Don't show this again")
    dontshowCheckBox.addEventListener('stateChanged',()=> {
        const config = require('../config.json')
        if(dontshowCheckBox.checkState()) {
            config.showTutorialonStart = false;
        }
        else {
            config.showTutorialonStart = true;
        }
        fs.writeFileSync('./src/config.json', JSON.stringify(config, null, 2));
    });

    nextButton.setText("Next >>")
    nextButton.addEventListener('clicked',()=>{
        if(currentSlide < slidePaths.length-1) {
            currentSlide++;
            win.setWindowTitle("Tutorial Slide " + (currentSlide+1) + "/" + slidePaths.length)
            imageLabel.setPixmap(new QPixmap(slidePaths[currentSlide]).scaled(1000,800,1,1));
            backButton.setEnabled(true);
            if(currentSlide == slidePaths.length-1) {
                nextButton.setEnabled(false);
                closeButton.setEnabled(true);
                closeButton.setInlineStyle("background-color:LawnGreen;")
            }
        }
    })

    backButton.setText("<< Back")
    backButton.setEnabled(false);
    backButton.addEventListener('clicked',()=>{
        if(currentSlide > 0) {
            currentSlide--;
            win.setWindowTitle("Tutorial Slide " + (currentSlide+1) + "/" + slidePaths.length)
            imageLabel.setPixmap(new QPixmap(slidePaths[currentSlide]).scaled(1000,800,1,1));
            nextButton.setEnabled(true);
            //closeButton.setEnabled(false);
            //closeButton.setInlineStyle("")
            if(currentSlide == 0) {
                backButton.setEnabled(false);
            }
        }
    })

    rootView.setLayout(rootLayout);
    rootLayout.addWidget(imageLabel);
    rootLayout.addWidget(controlField);

    controlField.setLayout(controlLayout);
    controlLayout.addWidget(closeButton)
    controlLayout.addWidget(dontshowCheckBox)
    controlLayout.addWidget(nextButton);
    controlLayout.addWidget(backButton);

    win.setCentralWidget(rootView)
    win.show();
    win.raise();
    win.addEventListener(WidgetEventTypes.Close, () => {
        cb();
        (global as any).initWindow.show();
        (global as any).initWindow.raise();
      });
    (global as any).tutorialWindow = win;
}

export function maximizeTutorial() {
    win.raise()
    win.setWindowState(8)
}