import {QFileDialog,FileMode, QWidget} from '@nodegui/nodegui'
import { Dock } from "@nodegui/os-utils";

export function openUsedFileDialog(caption:string="Select",directory?:string,mode?:any) {
        Dock.hide();
        const usedFileDialog = new QFileDialog(new QWidget(),caption,directory);
        if(mode) {
            usedFileDialog.setFileMode(FileMode.Directory);
        }
        else {
            usedFileDialog.setOption(16)
            usedFileDialog.setOption(32)
            usedFileDialog.setOption(64)
            usedFileDialog.setFileMode(FileMode.ExistingFiles);
        }
        //usedFileDialog.setFileMode(FileMode.ExistingFiles);
        usedFileDialog.exec();
        usedFileDialog.raise();
        (<any> process).send(usedFileDialog.selectedFiles())
}

openUsedFileDialog(process.argv[2] ? process.argv[2] : undefined, process.argv[3] ? process.argv[3] : undefined,process.argv[4] ? process.argv[4] : undefined);