import {LdpMementoClient, Fact} from "@i5/factlib.js/index";
import {ActivityView} from "./ActivityView"
import {downloadResource} from "./provView"
import {Namespaces} from "@i5/factlib-utils"
import {Stream} from "stream";
const service = new LdpMementoClient();
const pat = require('path')
const mime = require('mime-types')
const config = require('../config.json')
const idFactory = service.getIDFactory();
const RevisionRestorer = require('../LocalChangeManager/restoreRevision/restoreRevision')
const dLogoPath = pat.join(__dirname,"/assets/download.png");
const rLogoPath = pat.join(__dirname,"/assets/restore.png");
import {openWindows} from './provView'
import { Info } from "@nodegui/os-utils";

//-------------------------------------------------------------------------------

const { QMainWindow,QApplication, QTabWidget, WidgetEventTypes,QTreeWidgetItem,QIcon, QLabel, QPixmap,QTreeWidget, QPushButton, QToolButton, QMessageBox, ButtonRole } = require("@nodegui/nodegui");

//const downloadIcon = new QIcon("/Users/lmueller/GitLab/factfuse/assets/download.png")
const downloadIcon = new QIcon(dLogoPath)
const restoreIcon = new QIcon(rLogoPath)

export class ActivityHistory {
    uri: string;
    highlightedRevision: string;
    height = 100;
    win = new QMainWindow();
    tree = new QTreeWidget();
    tabWidget = new QTabWidget();
    actButtons: typeof QPushButton = []
    downloadButtons: typeof QPushButton = [];
    restoreButtons: typeof QPushButton = [];

    constructor(uri:string,revisionID="") {
        this.uri = uri;
        this.highlightedRevision = revisionID;
        this.checkIfExists();
    }

    async checkIfExists() {
        const resourceID = idFactory.parseURIToResourceID(this.uri)
        const exists = await service.isResourceExistent(resourceID);
        if(!exists) {
            const messageBox = new QMessageBox();
            messageBox.setText('Resource does not exist anymore.')
            messageBox.setInformativeText('Resource has been deleted. \n You can download this revision to access it.');
            const accept = new QPushButton();
            accept.setText('Ok');
            messageBox.addButton(accept, ButtonRole.AcceptRole);
            messageBox.exec();
        }
        else {
            this.createWindow(this.uri);   
        }
    }

    async createWindow(uri:string) {
        const resourceID = idFactory.parseURIToResourceID(uri)

        this.win.setWindowTitle(resourceID.resourceID + " | History");
        this.win.setObjectName(resourceID.resourceID)
        this.win.addEventListener(WidgetEventTypes.Close, () => {
            openWindows.splice(openWindows.map((x:any)=>x.objectName()).indexOf(resourceID.resourceID),1)
        });
        this.win.addEventListener(WidgetEventTypes.PaletteChange, () => {
            if(Info.isDarkMode()) {
                this.tree.setStyleSheet(`QTreeWidget::item:!selected { border-bottom: 1px solid gray;}
                QTreeWidget::item:last { color:green;}`);
            }
            else {
                this.tree.setStyleSheet(`QTreeWidget::item:!selected { border-bottom: 1px solid black;}
                QTreeWidget::item:last { color:green;}`);
            }
        });

        this.tree.setColumnCount(5)
        this.tree.setHeaderLabels(["#","Time", "Server", "Path", "Inspect"])
    
        this.tree.setColumnWidth(0,75)
        this.tree.setColumnWidth(1,400)
        this.tree.setColumnWidth(2,175)
        this.tree.setColumnWidth(3,175)
        this.tree.setColumnWidth(4,70)
        if(Info.isDarkMode()) {
            this.tree.setStyleSheet(`QTreeWidget::item:!selected { border-bottom: 1px solid gray;}
            QTreeWidget::item:last { color:green;}
            QTreeView::item { padding: 10px }`);
        }
        else {
            this.tree.setStyleSheet(`QTreeWidget::item:!selected { border-bottom: 1px solid black;}
            QTreeWidget::item:last { color:green;}
            QTreeView::item { padding: 10px }`);
        }

        this.win.setCentralWidget(this.tree);
        this.win.resize(900,250)
        this.win.addEventListener(WidgetEventTypes.PaletteChange, () => {
          if(Info.isDarkMode()) {
            for(const button of this.downloadButtons) {
                button.setIcon(new QIcon(pat.join(__dirname,"/assets/downloadLight.png")));
            }
            for(const button of this.restoreButtons) {
                button.setIcon(new QIcon(pat.join(__dirname,"/assets/restoreLight.png")));
            }
          }
          else {
            for(const button of this.downloadButtons) {
                button.setIcon(new QIcon(pat.join(__dirname,"/assets/download.png")));
            }
            for(const button of this.restoreButtons) {
                button.setIcon(new QIcon(pat.join(__dirname,"/assets/restore.png")));
            }
          }
        });
        this.win.show();
        (global as any).revGUI = this.win;
        openWindows.push(this.win)

        this.addRevisionItems(uri)
    }

    async addRevisionItems(uri:string) {
        const resourceID = idFactory.parseURIToResourceID(uri)
        const revisionList = await service.getRevisionListByID(resourceID);
        revisionList.reverse()
        var i = 0
        const treeItems = []

        for(const element of revisionList) {
            const activityFact = await service.getSpecificActivityRevision(element.factID)
            //add the revisions
            treeItems[i] = new QTreeWidgetItem(this.tree, [(i+1).toString()+".",new Date(element.factID.revisionID).toLocaleString(), element.factID.authorityID, element.factID.resourceID])
            if(this.highlightedRevision == element.factID.revisionID) {
                treeItems[i].setSelected(true);
            }

            //Add RDF preview
            const previewTreeWidgetItem = new QTreeWidgetItem(treeItems[i], ["",activityFact.serialize()])
            
            this.actButtons[i] = new QToolButton();
            this.actButtons[i].setObjectName("act"+i)
            this.actButtons[i].setIcon(new QIcon(pat.join(__dirname,"/assets/inspect.png")));
            this.actButtons[i].setInlineStyle("border : none;")
            //this.actButtons[i].setText(activityFact.resourceID.resourceName)
            this.actButtons[i].addEventListener('clicked',()=>{
                if(!openWindows.map((x:any)=>x.objectName()).includes(activityFact.factID.toString())) {
                    new ActivityView(activityFact.factID.toString());
                }
                else { //if activity view is already open, raise it
                    const index = openWindows.map((x:any)=>x.objectName()).indexOf(activityFact.factID.toString());
                    openWindows[index].setWindowState(8);
                    openWindows[index].raise();
                }
            });
            //this.actButtons[i].addEventListener('clicked',()=>this.tabWidget.addTab(new ActivityView(activityID.toString()).rootView,new QIcon(),resourceID.resourceID));
            this.tree.setItemWidget(treeItems[i],4,this.actButtons[i])


            i++;
            //check if element has older revisions, else break. getRivisionListByID() sometimes returns artifacts
            if(!activityFact.queryStore(Namespaces.PROV(Namespaces.PROV_RELATION.WAS_REVISION_OF))[0]) {
                //break
            }
        }
        
    }

    async updateWindow(element:any, ) { //used to update the history. mainly after restoring
        const self =this;
        self.tree.clear();
        await RevisionRestorer.restoreRevision(element.factID).then(function() {
            //self.tree.clear();
            self.addRevisionItems(self.uri);
        })
    }

}

//-------------------------------------------------------------------------------



//-------------------------------------------------------------------------------

