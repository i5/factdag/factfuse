import { Commit } from '../LocalChangeManager/Commit'
import { CommitHandler } from '../LocalChangeManager/CommitHandler'
import { setTitleBarStyle } from '@nodegui/plugin-title-bar'
import { Info } from "@nodegui/os-utils";
import {fork} from 'child_process'
const fs = require('fs')
//const config = require("../config.json")
//-------------------------------------------------------------------------------

const { QMainWindow,QIcon, QToolButton,WidgetEventTypes,QStatusBar,QPushButton, QLabel,  QWidget,QGridLayout,ButtonRole, QBoxLayout,QLineEdit,QTextEdit,QGroupBox, QCheckBox,QFileDialog,FileMode,QListWidget,QListWidgetItem, QMessageBox } = require("@nodegui/nodegui");
//const mountpoint = config.mountpoint;
//const root = config.ldpAddress;
const pat = require('path')
var helpOpen = false;

export class CommitGUI {
    win = new QMainWindow();
    progressBar = new QStatusBar();
    rootView = new QWidget();
    topField = new QWidget(); //naming fields for activity and agent
    midField = new QWidget(); //group boxes for include, used, generated
    addRemoveButtonField = new QWidget();
    aRBFLayout = new QBoxLayout(1);
    topLayout = new QGridLayout();
    midLayout = new QGridLayout();
    rootLayout = new QBoxLayout(2);
    includeBoxLayout = new QBoxLayout(2);
    usedBoxLayout = new QBoxLayout(2);
    generatedBoxLayout = new QBoxLayout(2);

    //fileDialog = new QFileDialog(this.rootView,"Add prov:used Resource",mountpoint);
    
    activityEdit = new QLineEdit();
    activityLabel = new QLabel();
    messageLabel = new QLabel();
    addUsed = new QPushButton();
    removeUsed = new QPushButton();
    commit = new QPushButton();
    messageEdit = new QTextEdit();
    includeBox = new QGroupBox();
    usedBox = new QGroupBox();
    usedList = new QListWidget();
    generatedBox = new QGroupBox();
    selectAllCheckBox = new QCheckBox();
    
    helpButton = new QToolButton();

    selectedFiles:any = [];
    usedWidgets:any = [];
    usedFacts:any = [];
    generatedWidgets:any = [];
    generatedFacts:any = [];
    changesToCommit:any = [];

    constructor() {
        const config = JSON.parse(fs.readFileSync(pat.join(__dirname,'/config.json')));
        this.win.setWindowTitle(" Manual Upload: "+config.ldpAddress);
        this.win.resize(400,300)
        this.win.addEventListener(WidgetEventTypes.PaletteChange, () => {
            if(Info.isDarkMode()) {
                this.commit.setIcon(new QIcon(pat.join(__dirname,"/assets/saveLight.png")))
            }
            else {
                this.commit.setIcon(new QIcon(pat.join(__dirname,"/assets/save.png")))
            }
        });
        
        //this.rootLayout.setSpacing(0)
        this.rootView.setObjectName("rootView");
        this.rootView.setLayout(this.rootLayout);
        this.topField.setLayout(this.topLayout);
        this.midField.setLayout(this.midLayout);
        this.addRemoveButtonField.setLayout(this.aRBFLayout);
        this.rootLayout.addWidget(this.helpButton)
        this.rootLayout.addWidget(this.topField);
        this.rootLayout.addWidget(this.midField);
        this.midLayout.addWidget(this.addRemoveButtonField,1,1);
        this.midLayout.addWidget(this.selectAllCheckBox,1,0);

        
        //this.helpButton.setGeometry(50,50,100,100)
        this.helpButton.setText("?");
        this.helpButton.addEventListener('clicked',()=> {
            if(!helpOpen) {
                helpOpen = true;
                openHelpWindow(function() {
                  helpOpen = false;
                });
              }
        });

        //const usedFileDialog = new QFileDialog(this.rootView,"Add prov:used Resource",mountpoint);
        //usedFileDialog.setFileMode(FileMode.ExistingFiles);
        //this.fileDialog.setNameFilter('../mount');

        this.activityLabel.setBuddy(this.activityEdit);
        this.topLayout.addWidget(this.activityLabel,0,0)
        this.topLayout.addWidget(this.activityEdit,0,1);
        this.activityEdit.setPlaceholderText("e.g. 'Edit_A'")

        this.midLayout.setVerticalSpacing(0)
        this.midLayout.addWidget(this.includeBox,0,0)
        this.includeBox.setLayout(this.includeBoxLayout)
        this.includeBox.setTitle("Changes")

        this.midLayout.addWidget(this.usedBox,0,1);
        this.usedBox.setLayout(this.usedBoxLayout)
        this.usedBox.setTitle("Used")

        this.usedBoxLayout.addWidget(this.usedList)
        this.usedList.addEventListener('itemSelectionChanged',()=> {
            if(this.usedList.selectedItems().length > 0){
                this.removeUsed.setEnabled(true)
            }
            else {
                this.removeUsed.setEnabled(false)
            }
        });

        //this.midLayout.addWidget(this.generatedBox,0,2)
        this.generatedBox.setLayout(this.generatedBoxLayout)
        this.generatedBox.setTitle("prov:generated")
        this.generatedBox.setCheckable(true)
        this.generatedBox.setChecked(false)

        this.aRBFLayout.addWidget(this.addUsed,0,1)
        this.addUsed.setText("+")
        this.addUsed.addEventListener('clicked',()=> {
            const forked = fork(pat.join(__dirname,'dialogOpener.js'),['Add used Resources',config.mountpoint])
            forked.on('message', (m:any) => {
                try {
                    for(const element of m.map((x:any)=>x.split(pat.basename(config.mountpoint))[1].replace(pat.extname(x.split(pat.basename(config.mountpoint))[1]),""))) {
                        if(!this.selectedFiles.includes(element)) {
                            //console.log(element)
                            this.selectedFiles.push(element);
                        }
                    }

                    this.updateUsed();
                    forked.kill();
                }
                catch {
                    const messageBox = new QMessageBox();
                    messageBox.setText('Illegal input')
                    messageBox.setInformativeText("You can only select Resources located inside the mountpoint.")
                    const accept = new QPushButton();
                    accept.setText('Ok');
                    messageBox.addButton(accept, ButtonRole.AcceptRole);
                    messageBox.exec();
                }
            });
        });

        this.aRBFLayout.addWidget(this.removeUsed,0,0)
        this.removeUsed.setText("-")
        this.removeUsed.setEnabled(false)
        this.removeUsed.addEventListener('clicked',()=> {
            for(const element of this.usedList.selectedItems()) {
                this.selectedFiles.splice(this.selectedFiles.indexOf(element.text()),1)
            }
            this.updateUsed();
        });

        this.selectAllCheckBox.setText("Select all");
        this.selectAllCheckBox.setEnabled(false);

        this.rootLayout.addWidget(this.messageLabel);
        this.rootLayout.addWidget(this.messageEdit);
        this.rootLayout.insertWidget(-1,this.commit,1);
        this.commit.setText("Upload")
        if(Info.isDarkMode()) {
            this.commit.setIcon(new QIcon(pat.join(__dirname,"/assets/saveLight.png")))
        }
        else {
            this.commit.setIcon(new QIcon(pat.join(__dirname,"/assets/save.png")))
        }
        this.commit.addEventListener('clicked',()=> {
            if(this.changesToCommit.filter((x:any)=>x>0).length > 0) {
                this.sendCommit();
            }
            else { 
                const messageBox = new QMessageBox();
                messageBox.setText('Nothing to commit. ')
                const accept = new QPushButton();
                accept.setText('Ok');
                messageBox.addButton(accept, ButtonRole.AcceptRole);
                messageBox.exec();
                }
        });

        this.rootLayout.addWidget(this.progressBar)
        this.progressBar.showMessage("Committing local changes to the LDP...")
        this.progressBar.hide();

        this.activityLabel.setText("Title: ");
        this.messageLabel.setText("Comment:");

        this.loaduncommittedChanges();

        this.win.resize(400,300)
        
        this.win.setCentralWidget(this.rootView);
        this.win.show();
        this.win.raise();
        (global as any).commitWindow = this.win;
    }

    loaduncommittedChanges() {
        const config = JSON.parse(fs.readFileSync(pat.join(__dirname,'/config.json')));
        const fsH = require('../FileSystemHandler/fileSystemHandler')
        const uncommittedChanges = {
            "Add": fsH.tempFiles.map((x:any)=>x.replace(pat.extname(x),"")).filter((y:any)=>!y.includes("/.")),
            "AddBin": fsH.tempBinaryFiles.filter((y:any)=>!y.includes("/.")),
            "AddDir": fsH.tempDirectories.filter((y:any)=>!y.includes("/.")),
            "Remove": fsH.deletedResources/*.map((x:any)=>x.replace(pat.extname(x),""))*/
          }

        var i = 0;
        var stateList = new Array(uncommittedChanges.Add.length + uncommittedChanges.AddBin.length + uncommittedChanges.AddDir.length +uncommittedChanges.Remove.length);
        const checkBoxList:any = [];

        if(uncommittedChanges.AddDir.length > 0) {
            const newDirsLabel = new QLabel();
            newDirsLabel.setText("New directories:");
            newDirsLabel.setInlineStyle("color:green; text-decoration: underline;");
            this.includeBoxLayout.addWidget(newDirsLabel);
        }
        for(const change of uncommittedChanges.AddDir) {
            const index = i;
            stateList[index] = 0;
            const box = new QCheckBox();
            box.setText("+ : " + change);
            box.addEventListener('stateChanged',()=>{stateList[index] = box.checkState();this.changesToCommit = stateList.map(x=>x/2);})
            this.includeBoxLayout.addWidget(box);
            checkBoxList.push(box);
            i++;
        }

        if(uncommittedChanges.Add.length > 0 || uncommittedChanges.AddBin.length > 0) {
            const newFilesLabel = new QLabel();
            newFilesLabel.setText("Files and edits:")
            newFilesLabel.setInlineStyle("color:green; text-decoration: underline;");
            this.includeBoxLayout.addWidget(newFilesLabel);
        }
        for(const change of uncommittedChanges.Add) {
            const index = i;
            stateList[index] = 0;
            const box = new QCheckBox();
            box.setText("+ : " + change + config.rdfExtension);
            box.addEventListener('stateChanged',()=>{stateList[index] = box.checkState();this.changesToCommit = stateList.map(x=>x/2);})
            this.includeBoxLayout.addWidget(box);
            checkBoxList.push(box);
            i++;
        }

        for(const change of uncommittedChanges.AddBin) {
            const index = i;
            stateList[index] = 0;
            const box = new QCheckBox();
            box.setText("+ : " + change);
            box.addEventListener('stateChanged',()=>{stateList[index] = box.checkState();this.changesToCommit = stateList.map(x=>x/2);})
            this.includeBoxLayout.addWidget(box);
            checkBoxList.push(box);
            i++;
        }

        if(uncommittedChanges.Remove.length > 0) {
            const removalsLabel = new QLabel();
            removalsLabel.setText("Removals:")
            removalsLabel.setInlineStyle("color:red; text-decoration: underline;");
            this.includeBoxLayout.addWidget(removalsLabel);
        }
        for(const change of uncommittedChanges.Remove) {
            const index = i;
            stateList[index] = 0;
            const box = new QCheckBox();
            box.setText("- : " + change);
            box.addEventListener('stateChanged',()=>{stateList[index] = box.checkState();this.changesToCommit = stateList.map(x=>x/2) ;})
            this.includeBoxLayout.addWidget(box);
            checkBoxList.push(box);
            i++;
        }

        if(stateList.length > 0) this.selectAllCheckBox.setEnabled(true);
        this.selectAllCheckBox.addEventListener('stateChanged',()=>{
            const checked = this.selectAllCheckBox.checkState();
            for(const box of checkBoxList) {
                box.setChecked(!checked)
                box.click();
            }
        });
    }

    updateUsed() {
        const config = JSON.parse(fs.readFileSync(pat.join(__dirname,'/config.json')));
        this.usedFacts = [];
        var usedArray:any = [];
        //clear the displayed labels and display the updated lists
        this.usedList.clear();
        for(const element of this.selectedFiles) {
            if(!usedArray.includes(element)) {
                usedArray.push(element)
                const label = new QListWidgetItem();
                label.setText(element);
                this.usedWidgets.push(label);
                this.usedFacts.push({authority:config.ldpAddress,facts:[element]})
                this.usedList.addItem(label)
            }
        }
    }

    async sendCommit() {
        if(this.activityEdit.text()!="") {
            this.progressBar.show();
            this.commit.setEnabled(false)
            const commit = new Commit(this.activityEdit.text().replace(" ","_"),this.messageEdit.toPlainText(),this.changesToCommit,this.usedFacts,this.generatedFacts);
            console.log(commit)
            var self = this;
            try {
                (global as any).systemStatus = 0;
                if(Info.isDarkMode()|| process.platform=="linux") {
                (global as any).tray.setIcon(new QIcon(pat.join(__dirname,"/assets/logoWorkingLight.png")));
                }
                else {
                (global as any).tray.setIcon(new QIcon(pat.join(__dirname,"/assets/logoWorking.png")));
                }
                (global as any).trayStatus.setEnabled(false);
                (global as any).trayStatus.setIcon(new QIcon(pat.join(__dirname,"/assets/working.png")));
                (global as any).trayStatus.setText('Syncing...');
                await(new CommitHandler(commit).commitSelectedChanges()).then(function(){
                    (global as any).systemStatus = 1;
                    if(Info.isDarkMode()|| process.platform=="linux") {
                    (global as any).tray.setIcon(new QIcon(pat.join(__dirname,"/assets/logoMountedLight.png")));
                    }
                    else {
                    (global as any).tray.setIcon(new QIcon(pat.join(__dirname,"/assets/logoMounted.png")));
                    }
                    (global as any).trayStatus.setEnabled(false);
                    (global as any).trayStatus.setIcon(new QIcon(pat.join(__dirname,"/assets/running.png")));
                    (global as any).trayStatus.setText("factFUSE is running")
                    setTimeout(function(){self.win.close();},1000);
                });
            }
            catch(e) {
                (global as any).systemStatus = -1;
                (global as any).tray.showMessage("Error during commit: ",e+'',new QIcon(pat.join(__dirname,"/assets/error.png")),3000);
                if(Info.isDarkMode()|| process.platform=="linux") {
                (global as any).tray.setIcon(new QIcon(pat.join(__dirname,"/assets/logoErrorLight.png")));
                }
                else {
                (global as any).tray.setIcon(new QIcon(pat.join(__dirname,"/assets/logoError.png")));
                }
                (global as any).trayStatus.setIcon(new QIcon(pat.join(__dirname,"/assets/error.png")));
                (global as any).trayStatus.setText(''+e);
                console.log(e);
                const messageBox = new QMessageBox();
                messageBox.setText('Commit failed')
                messageBox.setInformativeText('There was an error while trying to commit to the LDP.');
                messageBox.setDetailedText(e.toString())
                const accept = new QPushButton();
                accept.setText('Ok');
                messageBox.addButton(accept, ButtonRole.AcceptRole);
                messageBox.exec();

                this.progressBar.hide();
                this.commit.setEnabled(true)
            }
        }
        else {
            const messageBox = new QMessageBox();
            messageBox.setText('Title can\'t be empty.')
            messageBox.setInformativeText('Please enter a title for the commit.');
            const accept = new QPushButton();
            accept.setText('Ok');
            messageBox.addButton(accept, ButtonRole.AcceptRole);
            messageBox.exec();
        }
    }
}

function openHelpWindow(cb:any) {
    const helpWindow = new QMainWindow();
    helpWindow.setWindowTitle("Help");
    const helpView = new QWidget();
    const helpLayout = new QGridLayout();
    helpLayout.setVerticalSpacing(25)
    helpView.setLayout(helpLayout)

    const title = new QLabel();
    title.setText("\n Title:   ")
    title.setInlineStyle("font-weight: bold;")
    const helpTitle = new QLabel();
    helpTitle.setText("\n Enter a title for your upload activity.")
    title.setBuddy(helpTitle)

    const changesLabel = new QLabel();
    changesLabel.setText("Changes:   ")
    changesLabel.setInlineStyle("font-weight: bold;");
    const helpChanges = new QLabel();
    helpChanges.setText("Select which of your local changes you want to upload.")

    const usedLabel = new QLabel();
    usedLabel.setText("Used:   ")
    usedLabel.setInlineStyle("font-weight: bold;");
    const helpUsed = new QLabel();
    helpUsed.setText("Optional. Choose files to indicate that they were used in the selected changes.")

    const commentLabel = new QLabel();
    commentLabel.setText("Comment:   ")
    commentLabel.setInlineStyle("font-weight: bold;");
    const helpComment = new QLabel();
    helpComment.setText("Optional. Enter a comment to describe the upload activity. \nCan be useful to give more insight into changes.")

    helpLayout.addWidget(helpTitle,0,1)
    helpLayout.addWidget(title,0,0)
    helpLayout.addWidget(helpChanges,1,1)
    helpLayout.addWidget(changesLabel,1,0)
    helpLayout.addWidget(helpUsed,2,1)
    helpLayout.addWidget(usedLabel,2,0)
    helpLayout.addWidget(helpComment,3,1)
    helpLayout.addWidget(commentLabel,3,0)

    helpWindow.setCentralWidget(helpView)
    helpWindow.show();
    helpWindow.addEventListener(WidgetEventTypes.Close, () => {
        cb();
      });
    setTitleBarStyle(helpWindow.native, 'hidden'); // or hiddenInset
    (global as any).commitHelpWindow = helpWindow;
}