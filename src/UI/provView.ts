import {LdpMementoClient, Fact, Activity} from "@i5/factlib.js/index";
import {ActivityView} from "./ActivityView";
import {RevisionView} from "./RevisionView";
import {ActivityHistory} from "./ActivityHistory";
import {Stream} from 'stream';
const service = new LdpMementoClient();
const idFactory = service.getIDFactory();
const fs = require("fs")
const pat = require("path");
const mime = require('mime-types')
const spawn = require('child_process');
export const openWindows:any = [];

const { QPushButton,QMessageBox,ButtonRole} = require("@nodegui/nodegui");

export async function downloadResource(fact:Fact) {
    const config = JSON.parse(fs.readFileSync(pat.join(__dirname,'/config.json')));
    try {
        const bFact = await service.getSpecificBinaryFact(fact.factID)
        if (bFact.binaryContent instanceof Stream) {
            const extension = '.'+mime.extension(bFact.contentType);
            bFact.binaryContent.pipe(fs.createWriteStream(pat.join(config.output,fact.resourceID.resourceName+"_?"+fact.factID.revisionID +extension)));
            const messageBox = new QMessageBox();
            messageBox.setText('Download successful!')
            messageBox.setInformativeText('Saved File to: \n' + config.output);
            const accept = new QPushButton();
            accept.setText('Ok');
            const goTo = new QPushButton();
            goTo.setText('Open Folder');
            goTo.addEventListener('clicked', () => spawn.exec('open '+config.output))
            messageBox.addButton(goTo, ButtonRole.AcceptRole)
            messageBox.addButton(accept, ButtonRole.AcceptRole);
            messageBox.exec();
        }
    }
    catch {
        fs.writeFile(pat.join(config.output,fact.resourceID.resourceName+config.rdfExtension),fact.serialize(), function (err:any) {
            if (err) {
                const messageBox = new QMessageBox();
                messageBox.setText('Downloaded failed.');
                messageBox.setInformativeText('Try configuring another output folder.')
                messageBox.setDetailedText(err.toString())
                const accept = new QPushButton();
                accept.setText('Ok');
                messageBox.addButton(accept, ButtonRole.AcceptRole);
                messageBox.exec();
            }
            else {
                const messageBox = new QMessageBox();
                messageBox.setText('Download successful!')
                messageBox.setInformativeText('Saved File to: \n' + config.output);
                const accept = new QPushButton();
                accept.setText('Ok');
                const goTo = new QPushButton();
                goTo.setText('Open Folder');
                goTo.addEventListener('clicked', () => spawn.exec('open '+config.output))
                messageBox.addButton(goTo, ButtonRole.AcceptRole)
                messageBox.addButton(accept, ButtonRole.AcceptRole);
                messageBox.exec();
            }
        });
    }
}

async function start(uri: string) {
    try {
        console.log(uri)
        //const config = (global as any).ffconfig;
        //const config = require("../config.json")
        const config = JSON.parse(fs.readFileSync(pat.join(__dirname,'/config.json')));
        uri = pat.join(config.ldpAddress, uri.split(pat.basename(config.mountpoint))[1].replace(pat.extname(uri.split(pat.basename(config.mountpoint))[1]),""));
        const resourceID = idFactory.parseURIToResourceID(uri)
        const exists = await service.isResourceExistent(resourceID);
        if(!exists) {
            const messageBox = new QMessageBox();
            messageBox.setText('Resource not found.')
            messageBox.setInformativeText(uri + ': This resource does not exist on \n' + config.ldpAddress + '\nTry commiting local changes.');
            const accept = new QPushButton();
            accept.setText('Ok');
            messageBox.addButton(accept, ButtonRole.AcceptRole);
            messageBox.exec();
        }
        else {
            const resource = await service.getLastResourceRevision(resourceID)
            if(resource instanceof Activity) {
                //new ActivityView(uri)
                //new RevisionView(uri)
                new ActivityHistory(uri)
            }
            else if(resource instanceof Fact) {
                new RevisionView(uri)
            }
            else {
                const messageBox = new QMessageBox();
                messageBox.setText('Bad Resource')
                messageBox.setInformativeText('This resource represents neither a fact or activity.');
                const accept = new QPushButton();
                accept.setText('Ok');
                messageBox.addButton(accept, ButtonRole.DestructiveRole);
                messageBox.exec();
                process.exit();
            }
        }
    }
    catch(e) {
        const messageBox = new QMessageBox();
        messageBox.setText('Bad Resource')
        messageBox.setInformativeText('This resource appears to be outside the mountpoint. '+e +" "+ __dirname);
        //messageBox.setInformativeText(pat.join(__dirname,'/config.json'));
        const accept = new QPushButton();
        accept.setText('Ok');
        messageBox.addButton(accept, ButtonRole.DestructiveRole);
        messageBox.exec();
        process.exit();
    }
}

start(process.argv[2])