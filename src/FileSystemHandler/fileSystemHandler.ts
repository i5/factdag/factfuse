const Fuse = require('fuse-native');
const pat = require('path');
const fs = require("fs");
import {Stream} from "stream";
import {Commit} from '../LocalChangeManager/Commit';
import {CommitHandler} from '../LocalChangeManager/CommitHandler'
import {QIcon} from '@nodegui/nodegui'
import {LdpMementoClient, Fact, Directory} from "@i5/factlib.js/index";
const service = new LdpMementoClient();
//const config = require("../config.json")
const config = JSON.parse(fs.readFileSync(pat.join(__dirname,'/config.json')));
const idFactory = service.getIDFactory();
var root = config.ldpAddress;
const mime = require('mime-types')
const rdfExtension = config.rdfExtension;
import {File} from './File';
import { IResourceID } from "@i5/factlib.js/src/datamodels/interfaces/IResourceID";
var fd = 42;

async function listContent(uri:string) {
  const rID = idFactory.parseURIToResourceID(uri)
  const dir = await service.getDirectory(rID)
  const a = await listResources(dir)
  const b = await listDirectories(dir)
  const res = a.concat(b);
  return res
}

async function listDirectories(dir:Directory) {
  const containedDirectories = dir.getContainedDirectoriesList()
  for(const directory of containedDirectories) {
    if(!directories.includes(directory.resourceID.slice(0,-1))) {
      directories.push(directory.resourceID.slice(0,-1))
      if(tempDirectories.includes(directory.resourceID.slice(0,-1))) {
        tempDirectories.splice(tempDirectories.indexOf(directory.resourceID.slice(0,-1)),1)
      }
    }
  }
  const res = containedDirectories.map(x=>pat.basename(x.resourceID).toString())
  return res
}

async function listResources(dir:Directory) {
  const containedResources = dir.getContainedResourceList()

  const res = containedResources.map(async (resourceID) => {
    if(await service.isResourceBinary(resourceID) && !binaryFiles.map((x:any)=>x.replace(pat.extname(x),"")).includes(resourceID.resourceID)){
      createBinaryFile(resourceID)
      return resourceID.resourceName+'.'+(await service.getLastBinaryFactRevision(resourceID,"")).contentType?.split("/")[1];
    }
    else if(binaryFiles.map((x:any)=>x.replace(pat.extname(x),"")).includes(resourceID.resourceID)) {
      const cachedFile = fileCache[fileCache.map((x:any)=>(x.path).replace(pat.extname(x.path),"")).indexOf(resourceID.resourceID)]
      if(cachedFile) {
        return resourceID.resourceName+cachedFile.extension;
      }
      else {
        return resourceID.resourceName;
      }
    }
    else if(!files.includes(resourceID.resourceID+rdfExtension) && !binaryFiles.map((x:any)=>x.replace(pat.extname(x),"")).includes(resourceID.resourceID)) {
      createNonBinaryFile(resourceID);
      return resourceID.resourceName+rdfExtension;
    }
    else {
      return resourceID.resourceName+rdfExtension;
    }
  })
  return (await Promise.all(res));
}

async function createNonBinaryFile(resourceID:IResourceID) {
  const resource = await service.getLastResourceRevision(resourceID)
  var mtime = resource.factID.revisionID
  if(resource.constructor == Fact) {
    mtime = (await service.getLastFactRevision(resourceID)).factID.revisionID;
  }
  const store = resource.serialize()
  if(tempFiles.includes(resourceID.resourceID+rdfExtension)) {
      tempFiles.splice(tempFiles.indexOf(resourceID.resourceID+rdfExtension),1)
      const cachedFile = fileCache[fileCache.map((x:any)=>x.path).indexOf(resourceID.resourceID+rdfExtension)]
      if(cachedFile != undefined) {
        cachedFile.store = store;
        cachedFile.mtime = mtime;
      }
  }
  else {
      files.push(resourceID.resourceID+rdfExtension);
      fileCache.push(new File(resourceID.resourceID+rdfExtension,store,mtime));
  }
  resetAutoCommitter();
}
async function createBinaryFile(resourceID:IResourceID) {
      const binary = await service.getLastBinaryFactRevision(resourceID,"")
      const binaryStreamToBuffer = async (cb:any) =>{
        const buf:any = [];
        if (binary.binaryContent instanceof Stream) {
          binary.binaryContent.on("data", function (chunk:any) {
            buf.push(chunk)
          });
          binary.binaryContent.on("end", function () {
            cb(Buffer.concat(buf));
          });
        }
        else {
          cb(Buffer.from(binary.binaryContent));
        }
      }

      const extension = '.'+mime.extension(binary.contentType)
      const mtime = binary.factID.revisionID;
      const store = binary.serialize()
      if(tempBinaryFiles.includes(resourceID.resourceID+extension)) {
          tempBinaryFiles.splice(tempBinaryFiles.indexOf(resourceID.resourceID+extension),1)
          const cachedFile = fileCache[fileCache.map((x:any)=>x.path).indexOf(resourceID.resourceID+extension)]
          if(cachedFile != undefined) {
            await binaryStreamToBuffer(function(buffer:Buffer){
              cachedFile.binaryContent = buffer;
            });
            //cachedFile.binaryContent = binaryBuffer;
            cachedFile.store = store;
            cachedFile.mtime = mtime;
          }
      }
      else {
          binaryFiles.push(resourceID.resourceID+extension)
          await binaryStreamToBuffer(function(buffer:Buffer){
            fileCache.push(new File(resourceID.resourceID+extension,store,mtime,undefined,extension,buffer));
          });
          //fileCache.push(new File(resourceID.resourceID+extension,store,mtime,undefined,extension,binaryBuffer));
      }
      resetAutoCommitter();
}

async function updateFile(path:string) {
  const resourceID = idFactory.parseURIToResourceID(pat.join(root,path.replace(pat.extname(path),"")))
  const exists = await service.isResourceExistent(resourceID);
  if(exists && !deletedResources.includes(path)) {
    const resource = await service.getLastFactRevision(resourceID);
    const mtime = new Date(resource.factID.revisionID)
    const cachedFile = fileCache[fileCache.map((x:any)=>x.path).indexOf(path)]
    //console.log(path + ": " + mtime + " | " + cachedFile.mtime)
    if(mtime > new Date(cachedFile.mtime) && !tempFiles.includes(path) && !tempBinaryFiles.includes(path) && !deletedResources.includes(path)) { // update a local file with a newer version from the LDP
      console.log("update " + path)
      cachedFile.mtime = mtime;
      cachedFile.store = resource.serialize();
      //cachedFile.binaryContent = 
      cachedFile.lastUpdated = Date.now();

      ops.chmod(path,33188,function(){});

      resetAutoCommitter();
    }
    else if(mtime > new Date(cachedFile.mtime) && (tempFiles.includes(path))){ // server caught up with local change
      console.log(mtime + " | " + new Date(cachedFile.mtime))
      console.log(path + " now synchronized (rdf) on the LDP")
      tempFiles.splice(tempFiles.indexOf(path),1)
      resetAutoCommitter();
    }
    else if(mtime > new Date(cachedFile.mtime) && (tempBinaryFiles.includes(path))) { // server caught up with local change
      console.log(mtime + " | " + new Date(cachedFile.mtime))
      console.log(path + " now synchronized (binary) on the LDP")
      tempBinaryFiles.splice(tempBinaryFiles.indexOf(path),1)
      resetAutoCommitter();
    }
  }
  else if(!exists){
    if(deletedResources.includes(path)) {
      console.log("removed deleted")
      deletedResources.splice(deletedResources.indexOf(path,1))
      resetAutoCommitter();
    }
  }
}

async function resetAutoCommitter() { //update the local change list
  if(autoCommitter) {
      if(config.autoCommitEnabled) {
        autoCommitter.reset();
      }
    }
}


const directories = ['/']
const files:any = []
export const fileCache:any = []
const binaryFiles:any = []
export const tempFiles:any = []
export const tempBinaryFiles:any = []
export const tempDirectories:any = []
export const deletedResources:any = []


//--------------------------------------------------------------------------------------


export const ops = {
  statfs: function(path:any, cb:any) {
    //console.log("STAT!")
    return process.nextTick(cb, 0, {
      bsize: 100000,
      frsize: 100000,
      blocks: 1000000,
      bfree: 100000,
      bavail: 100000,
      files: 100000,
      ffree: 100000,
      favail: 100000,
      fsid: 100000,
      flag: 100000,
      namemax: 100000
    })
    
  },

  init: async function(cb:any) {
    console.log("Init")
    const resourceID = idFactory.createResourceID(root, 'facts/');
    const exists = await service.isResourceExistent(resourceID);
    if(exists == true) {
      await listContent(pat.join(root,'facts/'));
    }
    else {
      await listContent(pat.join(root,'/'))
    }

    return cb(0)
  },

  readdir: async function (path:any, cb:any) {
   //console.log('we have a directory to read:' + path)
    if(directories.includes(path)) {
      if(path=='/') {
        var contains = await listContent(pat.join(root,path)); //for starting at root, remove '/facts/' from directory array if using this
        //var contains = await listContent(pat.join(root,'facts/'));
      }
      else {
        var contains = await listContent(pat.join(root,path)+'/');
      }
      //check for any temporary local files in the directory
      const directoryTempFiles:any = []
      for(const element of tempFiles) {
        if (pat.dirname(element) == path/* && !files.includes(element)*/) {
          directoryTempFiles.push(pat.basename(element))
        }
      }
      for(const element of tempBinaryFiles) {
        if (pat.dirname(element) == path/* && !binaryFiles.includes(element)*/) {
          directoryTempFiles.push(pat.basename(element))
        }
      }
      for(const element of tempDirectories) {
        if (pat.dirname(element) == path/* && !files.includes(element)*/) {
          directoryTempFiles.push(pat.basename(element))
        }
      }
      return cb(null, contains.concat(directoryTempFiles).filter(x=>(x!=".well-known")&&(x!="activities")&&(x!="processes")&&(x!="broker1"+rdfExtension)&&(x!="broker2"+rdfExtension)))
    }
    
    else if(tempDirectories.includes(path)) {
      //check for any temporary local files in the directory
      const directoryTempFiles:any = []
      for(const element of tempFiles) {
        if (pat.dirname(element) == path && !files.includes(element)) {
          directoryTempFiles.push(pat.basename(element))
        }
      }
      for(const element of tempBinaryFiles) {
        if (pat.dirname(element) == path && !binaryFiles.includes(element)) {
          directoryTempFiles.push(pat.basename(element))
        }
      }
      for(const element of tempDirectories) {
        if (pat.dirname(element) == path && !files.includes(element)) {
          directoryTempFiles.push(pat.basename(element))
        }
      }

      return cb(null, (directoryTempFiles))
    }
    
    return cb(Fuse.ENOENT)
  },
/////////////////////////////////////////////////////////////////////////////////
  getattr: async function (path:string, cb:any) {
    //console.log('getattr(%s)' + path)
    //console.log(process.pid)
    if ((files.includes(path) || tempFiles.includes(path)) && !deletedResources.includes(path)){ //getattr for files
      const cachedFile = fileCache[fileCache.map((x:any)=>x.path).indexOf(path)]
      if(cachedFile != undefined) {
        const time = new Date(cachedFile.mtime);
        return process.nextTick(cb, 0, {
          mtime: time,
          atime: time,
          ctime: time,
          birthtime: time,
          nlink: 1,
          size: cachedFile.store.length,
          mode: cachedFile.mode,
          uid: process.getuid ? process.getuid() : 0,
          gid: process.getgid ? process.getgid() : 0
        })
      }
      else {
        return process.nextTick(cb, Fuse.ENOENT)
      }
    }
    else if((binaryFiles.includes(path) || tempBinaryFiles.includes(path)) && !deletedResources.includes(path)) {
      const cachedFile = fileCache[fileCache.map((x:any)=>x.path).indexOf(path)]
      if(cachedFile != undefined) {
        const time = new Date(cachedFile.mtime);
        const bSize = (cachedFile.binaryContent==undefined) ? 0: cachedFile.binaryContent.length;
        return process.nextTick(cb, 0, {
          mtime: time,
          atime: time,
          ctime: time,
          birthtime: time,
          nlink: 1,
          size: bSize,
          mode: cachedFile.mode,
          uid: process.getuid ? process.getuid() : 0,
          gid: process.getgid ? process.getgid() : 0
        })
      }
      else {
        return process.nextTick(cb, Fuse.ENOENT)
      }
    }
    else if((directories.includes(path) || tempDirectories.includes(path)) && !deletedResources.includes(path)){ //getattr for directories
      return process.nextTick(cb, 0, {
        mtime: new Date(),
        atime: new Date(),
        ctime: new Date(),
        nlink: 2,
        mode: 16871,
        uid: process.getuid ? process.getuid() : 0,
        gid: process.getgid ? process.getgid() : 0
      })
    }
    else {
      return process.nextTick(cb, Fuse.ENOENT)
    }
  },

  release: function (path:any, fd:any, cb:any) {
    //console.log("release " + path + fd)
    //path = path.replace(pat.extname(path),"")
    const cachedFile = fileCache[files.indexOf(path)]
    if(cachedFile && path.includes("/facts/")) {
      const timeSinceLastUpdate = Math.floor((Date.now() - cachedFile.lastUpdated)/1000) //in seconds
      if(timeSinceLastUpdate > 5) {
        cachedFile.lastUpdated = Date.now();
        updateFile(path)
      }
    }

    return process.nextTick(cb, 0)
  },

  access: function(path:any, mode:any, cb:any) {
    return process.nextTick(cb, 0)
  },

  unlink: function (path:any, cb:any) {
    //path = path.replace(pat.extname(path),"")

    console.log("unlink " + path)

    if(files.includes(path) || binaryFiles.includes(path)) {
      deletedResources.push(path)
    }
    if(tempFiles.includes(path)) {
      const index = tempFiles.indexOf(path)
      tempFiles.splice(index,1)
    }
    else if(tempBinaryFiles.includes(path)) {
      const index = tempBinaryFiles.indexOf(path)
      tempBinaryFiles.splice(index,1)
    }
    const cachedFile = fileCache[fileCache.map((x:any)=>x.path).indexOf(path)]
    fileCache.splice(fileCache.indexOf(cachedFile),1)
    resetAutoCommitter();
    return process.nextTick(cb, 0)
  },

  read: async function (path:any, fd:any, readBuffer:Buffer, len:any, pos:any, cb:any) {
    //console.log('read(%s, %d, %d, %d)', path, fd, len, pos)
    const cachedFile = fileCache[fileCache.map((x:any)=>x.path).indexOf(path)]

    if(binaryFiles.includes(path) || tempBinaryFiles.includes(path)) {
      cachedFile.binaryContent.slice(pos, pos + len).copy(readBuffer)
      return cb(readBuffer.length)
    }
    else if(files.includes(path) || tempFiles.includes(path)) {  
      const res = cachedFile.store.toString().slice(pos, pos + len)
      readBuffer.write(res)
      return process.nextTick(cb, res.length)
    }
    else {
      return process.nextTick(cb, Fuse.ENOENT)
    }
  },

  mkdir: async function(path:any, mode:any, cb:any) {
    console.log("mkdir: " + path)
    tempDirectories.push(path)
    if(deletedResources.includes(path)) {
        deletedResources.splice(deletedResources.indexOf(path),1)
    }
    ////////////////////
    resetAutoCommitter();
    ////////////////////
    return process.nextTick(cb, 0)
  },

  rmdir: async function(path:any, cb:any) {
    console.log("rmdir:" + path)
    const resourceID = idFactory.createResourceID(root, path);
    const exists = await service.isResourceExistent(resourceID);
    if(exists == true) {
      deletedResources.push(path)
      resetAutoCommitter();
      return process.nextTick(cb, 0)
    }
    else {
      const index = tempDirectories.indexOf(path)
      tempDirectories.splice(index,1)
      resetAutoCommitter();
      return process.nextTick(cb, 0)
    }
  },

  /*opendir: function(path:string, flags:any, cb:any) {
    return process.nextTick(cb,0)
  },*/
  releasedir: function(path:string, fd:any, cb:any) {
    //console.log("RD!!!!!!: " + path)
    return process.nextTick(cb,0)
  },

  rename: async function(src:any, dest:any, cb:any) {
    console.log("rename " + src + " | " + dest)
    if(dest.includes(" ")) {
      return process.nextTick(cb, Fuse.EROFS)
    }
    const cachedFile = fileCache[fileCache.map((x:any)=>x.path).indexOf(src)]
    console.log(cachedFile.extension)

    const oldCache = fileCache[fileCache.map((x:any)=>x.path).indexOf(dest)]
    

    if(files.includes(src)) {
        if(deletedResources.includes(dest)) {
            deletedResources.splice(deletedResources.indexOf(dest),1)
            if(!tempFiles.includes(dest)) {
              tempFiles.push(dest);
            }
            deletedResources.push(src)
            cachedFile.path = dest;
            cachedFile.extension = pat.extname(dest);
            cachedFile.mtime = new Date();
            resetAutoCommitter();
            //return process.nextTick(cb, 0)
        }
        else {
            if(!tempFiles.includes(dest)) {
              tempFiles.push(dest);
            }
            cachedFile.path = dest;
            cachedFile.extension = pat.extname(dest);
            cachedFile.mtime = new Date();
            deletedResources.push(src)
            resetAutoCommitter();
            //return process.nextTick(cb, 0)
        }
    }
    else if(binaryFiles.includes(src)) {
      if(deletedResources.includes(dest)) {
        deletedResources.splice(deletedResources.indexOf(dest),1)
        deletedResources.push(src)
        if(!tempBinaryFiles.includes(dest)) {
          tempBinaryFiles.push(dest);
        }
        cachedFile.path = dest;
        cachedFile.extension = pat.extname(dest);
        cachedFile.mtime = new Date();
        resetAutoCommitter();
        //return process.nextTick(cb, 0)
      }
      else {
        if(!tempBinaryFiles.includes(dest)) {
          tempBinaryFiles.push(dest);
        }
        cachedFile.path = dest;
        cachedFile.extension = pat.extname(dest);
        cachedFile.mtime = new Date();
        deletedResources.push(src)
        resetAutoCommitter();
        //return process.nextTick(cb, 0)
      }
    }
    else if(tempFiles.includes(src)) {
      console.log("tempfiles includes src")
        if(deletedResources.includes(dest)) {
            deletedResources.splice(deletedResources.indexOf(dest),1)
            if(!tempFiles.includes(dest)) {
              tempFiles.push(dest);
            }
            cachedFile.path = dest;
            cachedFile.extension = pat.extname(dest);
            cachedFile.mtime = new Date();
            tempFiles.splice(tempFiles.indexOf(src),1)
            resetAutoCommitter();
            //return process.nextTick(cb, 0)
        }
        else if(tempBinaryFiles.includes(dest) || binaryFiles.includes(dest)) { //  This is a special case for changes to binary files (or overwriting) in GNOME. A better solution would be to handle file extensions in addition to the path
          if(!tempBinaryFiles.includes(dest)) {
            tempBinaryFiles.push(dest);
          }
          tempFiles.splice(tempBinaryFiles.indexOf(src),1)
          cachedFile.path = dest
          //cachedFile.binaryContent = Buffer.concat([cachedFile.binaryContent,Buffer.from(cachedFile.store,'utf8').slice(0,cachedFile.store.length)]);
          cachedFile.store = oldCache.store
          cachedFile.extension = pat.extname(dest);
          cachedFile.mtime = new Date();
          resetAutoCommitter();
          console.log(cachedFile)
          //return process.nextTick(cb, 0)
        }
        else {
            if(!tempFiles.includes(dest)) {
              tempFiles.push(dest);
            }
            tempFiles.splice(tempFiles.indexOf(src),1)
            cachedFile.path = dest
            cachedFile.extension = pat.extname(dest);
            cachedFile.mtime = new Date();
            resetAutoCommitter();
            //return process.nextTick(cb, 0)
        }
    }
    else if(tempBinaryFiles.includes(src)) {
      if(deletedResources.includes(dest)) {
          deletedResources.splice(deletedResources.indexOf(dest),1)
          if(!tempBinaryFiles.includes(dest)) {
            tempBinaryFiles.push(dest);
          }
          cachedFile.path = dest;
          cachedFile.extension = pat.extname(dest);
          cachedFile.mtime = new Date();
          tempBinaryFiles.splice(tempBinaryFiles.indexOf(src),1)
          resetAutoCommitter();
          //return process.nextTick(cb, 0)
      }
      else {
          if(!tempBinaryFiles.includes(dest)) {
            tempBinaryFiles.push(dest);
          }
          tempBinaryFiles.splice(tempBinaryFiles.indexOf(src),1)
          cachedFile.path = dest
          cachedFile.extension = pat.extname(dest);
          cachedFile.mtime = new Date();
          resetAutoCommitter();
          //return process.nextTick(cb, 0)
      }
  }
    else if(tempDirectories.includes(src)) {
      tempDirectories[tempDirectories.indexOf(src)] = dest;
      resetAutoCommitter();
            return process.nextTick(cb, 0)
    }
    else if(directories.includes(src)) {
        return process.nextTick(cb, Fuse.EROFS)
    }

    //Remove old cache file if it exists
    if(oldCache) {
      console.log("remove old cached file of " + dest)
      fileCache.splice(fileCache.indexOf(oldCache),1)
    }
    return process.nextTick(cb, 0)
  },

  write: function(path:any, fd:any, buffer:any, length:any, position:any, cb:any) {
    console.log("write " + path + " | " + fd + " | " + length + " | " + position)

    if(deletedResources.includes(path)) {
        deletedResources.splice(deletedResources.indexOf(path),1)
    }
    if(files.includes(path) || tempFiles.includes(path)) {
      console.log("***** writing rdf ******")
      const cachedFile = fileCache[fileCache.map((x:any)=>x.path).indexOf(path)]
      const res = buffer.slice(position,length).toString();
      cachedFile.store = res;
      cachedFile.binaryContent = Buffer.concat([cachedFile.binaryContent,buffer.slice(0,length)]) //Experimental: hacky solution to gnome binary edits (in hope that binary contents of regular rdf files do not lead to confusion elsewhere in the system...)
      cachedFile.mtime = new Date();
    
      if(!tempFiles.includes(path)) {
          tempFiles.push(path)
      }
      resetAutoCommitter();
      return process.nextTick(cb,length)
    }
    else if(binaryFiles.includes(path) || tempBinaryFiles.includes(path)) {
      console.log("***** writing binary ******")
      const cachedFile = fileCache[fileCache.map((x:any)=>x.path).indexOf(path)]
      cachedFile.binaryContent = Buffer.concat([cachedFile.binaryContent,buffer.slice(0,length)])
      cachedFile.mtime = new Date();
      
      if(!tempBinaryFiles.includes(path)) {
          tempBinaryFiles.push(path)
      }
      resetAutoCommitter();
      return process.nextTick(cb,length)
    }
    else {
      return process.nextTick(cb,Fuse.ENOENT)
    }
  },

  create: async function(path:any, mode:any, cb:any) {
    if(deletedResources.includes(path)) {
        deletedResources.splice(deletedResources.indexOf(path),1)
    }
    if(pat.extname(path)==rdfExtension || path.includes('/.')) {
      if(!tempFiles.includes(path)) {
        console.log("create " + path)
        tempFiles.push(path)
        if(!fileCache.map((x:any)=>x.path).includes(path)) {
          fileCache.push(new File(path,""));
        }
        ///////////
        resetAutoCommitter();
        ////////////////////
        fd++;
        return process.nextTick(cb, 0, fd)
      }
    }
    else {
      if(!tempBinaryFiles.includes(path)) {
        console.log("create Binary " + path)
        tempBinaryFiles.push(path)
        if(!fileCache.map((x:any)=>x.path).includes(path)) {
          fileCache.push(new File(path,"",undefined,undefined,pat.extname(path)));
        }
        ///////////
        resetAutoCommitter();
        ////////////////////
        fd++;
        return process.nextTick(cb, 0, fd)
      }
    }
  },

  truncate: function(path:any, size:any, cb:any) {
    console.log("truncate " + path + " | " + size)
    const cachedFile = fileCache[fileCache.map((x:any)=>x.path).indexOf(path)]
    if(files.includes(path)|| tempFiles.includes(path)) {
      cachedFile.store = cachedFile.store.slice(0,size)
      return process.nextTick(cb, 0)
    }
    else if(binaryFiles.includes(path) || tempBinaryFiles.includes(path)) {
      cachedFile.binaryContent = cachedFile.binaryContent.slice(0,size);
      return process.nextTick(cb, 0)
    }

  },

  chown: function(path:any, uid:any, gid:any, cb:any) {
    console.log("chown " + path)
    return process.nextTick(cb, 0)
  },

  chmod: function(path:any,mode:any,cb:any) {
    console.log("chmod(%s,%s)",path,mode)
    if(files.includes(path) || tempFiles.includes(path) || binaryFiles.includes(path) || tempBinaryFiles.includes(path)) {
      const cachedFile = fileCache[fileCache.map((x:any)=>x.path).indexOf(path)]
      cachedFile.mode = mode;
      return process.nextTick(cb, 0)
    }
    else {
      return process.nextTick(cb, Fuse.EISDIR)
    }
  }

}


setInterval(function() {
  if(autoCommitter && autoCommitter.timerObj) {
    //if(autoCommitter.timerObj) {
      if((global as any).systemStatus == 1 && tempFiles.concat(tempBinaryFiles).concat(deletedResources).filter((y:any)=>!y.includes("/.")).length >0) {
          for(const tempFile of tempFiles.concat(tempBinaryFiles).concat(deletedResources).filter((y:any)=>!y.includes("/."))) {
            if(!tempFile.includes(" ") /*&& !directories.includes(tempFile)*/) {
              //console.log("go: " + tempFile)
              updateFile(tempFile);
            }
          }
      }
    //} 
  }
  else { //if manual committing is enabled
    if(tempFiles.concat(tempBinaryFiles).concat(deletedResources).filter((y:any)=>!y.includes("/.")).length >0) {
      for(const tempFile of tempFiles.concat(tempBinaryFiles).concat(deletedResources).filter((y:any)=>!y.includes("/."))) {
        if(!tempFile.includes(" ") /*&& !directories.includes(tempFile)*/) {
          //console.log("go: " + tempFile)
          updateFile(tempFile);
        }
      }
  }
  }
},500)

var autoCommitter:any;
(global as any).autoCommitter = autoCommitter;

export class FactFUSE {
  private fuse: typeof Fuse;
  private mountpoint:string;

  constructor(mountpoint: string) {
    const config = JSON.parse(fs.readFileSync(pat.join(__dirname,'/config.json')));
    if(autoCommitter) {
      autoCommitter.stop();
    }
    if(config.autoCommitEnabled) {
      autoCommitter = new AutoCommitter(config.commitInterval);
      console.log("AutoCommiter started")
    }
    this.mountpoint = mountpoint;
    root = config.ldpAddress;
    this.fuse = new Fuse(mountpoint, ops, {
      displayFolder: 'LDP', 
      debug: false,
      force: true,
      timeout: 5000,
      name: config.ldpAddress,
      mkdir:true,
      allow_root:true,
      local:false,
      auto_cache:true 
    })
  }

  public mount() {
    const self = this;
    this.fuse.mount(function (err:any) {
      if (err) {
        throw err
      }
      console.log('filesystem mounted on ' + self.mountpoint)
    })
  }


  public unmount(cb:any) {
    this.fuse.unmount(function(err:any){
      if (err) {
        console.log('filesystem not unmounted', err)
      } else {
        if(autoCommitter) {
          autoCommitter.stop();
        }
        console.log('filesystem unmounted')
        cb();
      }
    })
  }

  public startAutoCommitter() {
    if(autoCommitter) {
      autoCommitter.start();
    }
    else {
      autoCommitter = new AutoCommitter(config.commitInterval);
    }
  }
  public stopAutoCommitter() {
    autoCommitter.stop();
  }
}

import { Info } from "@nodegui/os-utils";

class AutoCommitter {
  timerObj:any;
  interval:number
  constructor(t:any) {
    const self = this;
    this.timerObj = setInterval(function () {
      const uncommittedChanges = {
        "Add": tempFiles.map((x:any)=>x.replace(pat.extname(x),"")).filter((y:any)=>!y.includes("/.")),
        "AddBin": tempBinaryFiles.filter((y:any)=>!y.includes("/.")),
        "AddDir": tempDirectories.filter((y:any)=>!y.includes("/.")),
        "Remove": deletedResources.map((x:any)=>x.replace(pat.extname(x),""))
      }
      const numberOfChanges = uncommittedChanges.Add.length + uncommittedChanges.AddBin.length + uncommittedChanges.Remove.length; // doesnt include directories. dirs only get pushed when they contain new resources
      const arr = new Array(numberOfChanges+uncommittedChanges.AddDir.length).fill(1); //array indicating all changes are staged
      const commit = new Commit("commit"+Date.now().toString(),"Automatic commit",arr);
      //console.log("***"+numberOfChanges)
      if(numberOfChanges>0) {
          self.stop()
          console.log(commit)
          self.sendCommit(commit)
      }
      else if(numberOfChanges==0) {
          (global as any).systemStatus = 1;
          if(Info.isDarkMode() || process.platform=="linux") {
            (global as any).tray.setIcon(new QIcon(pat.join(__dirname,"/assets/logoMountedLight.png")));
          }
          else {
            (global as any).tray.setIcon(new QIcon(pat.join(__dirname,"/assets/logoMounted.png")));
          }
          (global as any).trayStatus.setEnabled(false);
          (global as any).trayStatus.setIcon(new QIcon(pat.join(__dirname,"/assets/running.png")));
          (global as any).trayStatus.setText("factFUSE is running")
      }
  }, t);
    this.interval = t;
  }
  stop() {
    if (this.timerObj) {
        //console.log("stopped AutoCommit")
        clearInterval(this.timerObj);
        this.timerObj = null;
    }
    return this;
  }

  start() {
    if (!this.timerObj) {
        //console.log("started AutoCommit")
        const self = this;
        this.stop();
        this.timerObj = setInterval(function () {
          const uncommittedChanges = {
            "Add": tempFiles.map((x:any)=>x.replace(pat.extname(x),"")).filter((y:any)=>!y.includes("/.")),
            "AddBin": tempBinaryFiles.filter((y:any)=>!y.includes("/.")),
            "AddDir": tempDirectories.filter((y:any)=>!y.includes("/.")),
            "Remove": deletedResources.map((x:any)=>x.replace(pat.extname(x),""))
          }
          const numberOfChanges = uncommittedChanges.Add.length + uncommittedChanges.AddBin.length + uncommittedChanges.Remove.length; // doesnt include directories. dirs only get pushed when they contain new resources
          const arr = new Array(numberOfChanges+uncommittedChanges.AddDir.length).fill(1); //array indicating all changes are staged
          const commit = new Commit("commit"+Date.now().toString(),"Automatic commit",arr);
          //console.log(numberOfChanges)
          if(numberOfChanges>0) {
              self.stop();
              console.log(commit)
              self.sendCommit(commit)
          }
          else if(numberOfChanges==0) {
              (global as any).systemStatus = 1;
              if(Info.isDarkMode() || process.platform=="linux") {
                (global as any).tray.setIcon(new QIcon(pat.join(__dirname,"/assets/logoMountedLight.png")));
              }
              else {
                (global as any).tray.setIcon(new QIcon(pat.join(__dirname,"/assets/logoMounted.png")));
              }
              (global as any).trayStatus.setEnabled(false);
              (global as any).trayStatus.setIcon(new QIcon(pat.join(__dirname,"/assets/running.png")));
              (global as any).trayStatus.setText("factFUSE is running")
          }
      }, this.interval);
    }
    return this;
  }

  reset() {
    //console.log("reset AutoCommit")
    return this.stop().start();
  }

  async sendCommit(commit:Commit) {
    const self = this;
    try {
        (global as any).systemStatus = 0;
        if(Info.isDarkMode()|| process.platform=="linux") {
          (global as any).tray.setIcon(new QIcon(pat.join(__dirname,"/assets/logoWorkingLight.png")));
        }
        else {
          (global as any).tray.setIcon(new QIcon(pat.join(__dirname,"/assets/logoWorking.png")));
        }
        (global as any).trayStatus.setEnabled(false);
        (global as any).trayStatus.setIcon(new QIcon(pat.join(__dirname,"/assets/working.png")));
        (global as any).trayStatus.setText('Syncing...');
        
        await(new CommitHandler(commit).commitSelectedChanges()).then(function() {
            self.start();
            (global as any).systemStatus = 1;
            if(Info.isDarkMode()|| process.platform=="linux") {
              (global as any).tray.setIcon(new QIcon(pat.join(__dirname,"/assets/logoMountedLight.png")));
            }
            else {
              (global as any).tray.setIcon(new QIcon(pat.join(__dirname,"/assets/logoMounted.png")));
            }
            (global as any).trayStatus.setEnabled(false);
            (global as any).trayStatus.setIcon(new QIcon(pat.join(__dirname,"/assets/running.png")));
            (global as any).trayStatus.setText("factFUSE is running")
        })

    }
    catch(e) {
        (global as any).systemStatus = -1;
        (global as any).tray.showMessage("Error during sync",e+'',new QIcon(pat.join(__dirname,"/assets/error.png")),3000);
        if(Info.isDarkMode()|| process.platform=="linux") {
          (global as any).tray.setIcon(new QIcon(pat.join(__dirname,"/assets/logoErrorLight.png")));
        }
        else {
          (global as any).tray.setIcon(new QIcon(pat.join(__dirname,"/assets/logoError.png")));
        }
        (global as any).trayStatus.setIcon(new QIcon(pat.join(__dirname,"/assets/error.png")));
        (global as any).trayStatus.setText(''+e);
    
    }
  }
}