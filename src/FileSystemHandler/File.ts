const pat = require('path')
const config = require('../config.json')
const rdfExtension = config.rdfExtension;
export class File {
    public path:string;
    public resourceID:string;
    public extension:string;
    public store:string;
    public mtime:any;
    public mode:any;
    public binaryContent:Buffer;
    public lastUpdated:any;

    constructor(path:string, store:string = "", mtime = Date.now().toString(),mode=33188,extension:string = rdfExtension,binaryContent?:Buffer) {
        this.path = path;
        this.resourceID = path.replace(pat.extname(path),"")
        this.extension = extension;
        this.store = store;
        this.mtime = new Date(mtime);
        this.mode = mode
        this.lastUpdated = Date.now();

        if(!binaryContent) {
            this.binaryContent = Buffer.from('');
        }
        else {
            this.binaryContent = Buffer.from(binaryContent);
        }
    }

    setBinaryContent(content:Buffer) {
        this.binaryContent = Buffer.from(content);
    }
}