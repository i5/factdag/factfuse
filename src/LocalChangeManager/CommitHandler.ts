import {ProcessLifecycleManager,LdpMementoClient} from "@i5/factlib.js/index";
import { Commit } from './Commit'
const fs = require('fs')
const pat = require('path')
const service = new LdpMementoClient();
const ResourceCreater = require('./createResource/createResource')
const ResourceDeleter = require('./deleteResource/deleteResource')
const ContainerCreater = require('./createDirectory/createDirectory')


export class CommitHandler {
    commit:Commit;
    constructor(commit:Commit) {
        this.commit = commit;
    }

    async commitSelectedChanges() {
        const config = JSON.parse(fs.readFileSync(pat.join(__dirname,'/config.json')));

        const fsH = require('../FileSystemHandler/fileSystemHandler')
        const uncommittedChanges = {
            "Add": fsH.tempFiles.map((x:any)=>x.replace(pat.extname(x),"")).filter((y:any)=>!y.includes("/.")),
            "AddBin": fsH.tempBinaryFiles.filter((y:any)=>!y.includes("/.")),
            "AddDir": fsH.tempDirectories.filter((y:any)=>!y.includes("/.")),
            //"AddDir": fsH.tempDirectories,
            "Remove": fsH.deletedResources.map((x:any)=>x.replace(pat.extname(x),""))
          }

        const commitConfig = {         
            "authority": {
              "uri": config.ldpAddress
            },
            "process": {
              "checkLatest": false,
              "location": "/processes/"+config.userID,
              "uses": this.commit.usedEntities,
              "generated": this.commit.generatedEntities,
            },
            "activity": {
              "location": "/activities/"+this.commit.activity
            }
        }
        const pManager = new ProcessLifecycleManager(commitConfig);
        await pManager.start();
        const candidates = [];
        const binaryCandidates:any = [];
        const deletions:any = [];
        var i = 0;
        console.log(uncommittedChanges)
        for(const addedDir of uncommittedChanges.AddDir) {
            if(this.commit.listOfChanges[i] == 1) {
                console.log("mkdir: " + addedDir)
                ContainerCreater.createDirectory(addedDir)
            }
            i++;
        }
        for(const addedEntity of uncommittedChanges.Add) {
            if(this.commit.listOfChanges[i] == 1) {
                console.log("create: " + addedEntity)
                const cand = await ResourceCreater.createResourceCandidate(addedEntity)
                if(cand) {
                    console.log("cand")
                    candidates.push(cand)
                }
            }
            i++;
        }
        for(const addedBinary of uncommittedChanges.AddBin) {
            if(this.commit.listOfChanges[i] == 1) {
                console.log("create Binary: " + addedBinary)
                const cand = await ResourceCreater.createBinaryResourceCandidate(addedBinary)
                if(cand) {
                    candidates.push(cand)
                }
            }
            i++;
        }
        for(const deletedResource of uncommittedChanges.Remove) {
            if(this.commit.listOfChanges[i] == 1) {
                console.log("delete: " + deletedResource)
                deletions.push(deletedResource)
            }
            i++;
        }

        await pManager.execProcess(candidates,undefined,this.commit.message).then(async function(){
            for(const element of deletions) {
                await ResourceDeleter.deleteResource(element);
            }
        });     
    }
}