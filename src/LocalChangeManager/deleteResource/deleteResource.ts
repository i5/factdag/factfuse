import {LdpMementoClient} from "@i5/factlib.js/index";
const service = new LdpMementoClient();
const idFactory = service.getIDFactory();
const fs = require('fs');
const pat = require('path')


export async function deleteResource(path:string) {
    //const config = JSON.parse(fs.readFileSync(pat.join(__dirname,'../../config.json')));
    const config = JSON.parse(fs.readFileSync(pat.join(__dirname,'/config.json')));
    const authorityID = config.ldpAddress;
    console.log("deleting " + path)
    var resourceID = idFactory.createResourceID(authorityID, path)
    await service.deleteResource(resourceID)
}

//deleteResource('/dir/send')