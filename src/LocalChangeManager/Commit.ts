export class Commit {
    constructor(activity:string,message:string, listOfChanges:number[], usedEntities:any=[], generatedEntities:any=[]) {
        this.id = Date.now();
        this.activity = activity;
        this.message = message;
        this.listOfChanges = listOfChanges;
        this.usedEntities = usedEntities;
        this.generatedEntities = generatedEntities;
    }
    public id:any;
    public activity:string;
    message:string;
    listOfChanges:number[];
    usedEntities:any;
    generatedEntities:any;
}