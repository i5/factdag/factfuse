import {LdpMementoClient, FactCandidate} from "@i5/factlib.js/index";
import { BinaryFactCandidate } from "@i5/factlib.js/src/datamodels/factdag/candidates/binaryFactCandidate";
const service = new LdpMementoClient();
const idFactory = service.getIDFactory();
const fs = require('fs')
const pat = require('path')
const rdf = require('rdflib')
const mime = require('mime-types')


export async function createResourceCandidate(path:string) { //Creates RDF Source Fact Candidates
    const config = JSON.parse(fs.readFileSync(pat.join(__dirname,'/config.json')));
    const authorityID = config.ldpAddress;
    const mountpoint = config.mountpoint;
    const rdfExtension = config.rdfExtension;

    const resourceID = idFactory.createResourceID(authorityID, path);
    const isFact = await service.isResourceExistent(resourceID);
    //const content = fs.readFileSync(mountpoint+path+rdfExtension).toString();
    const store = rdf.graph();
    try {
    fs.readFile(mountpoint+path+rdfExtension, (err:any, data:any) => {
        try {
            rdf.parse(data.toString().replace(/\r?\n|\r/g,""), store, resourceID.resourceURI.toString(), 'text/turtle')
        }
        catch {
            
        }
      });
    }
    catch {
        return undefined;
    }
    //rdf.parse(content.replace(/\r?\n|\r/g,""), store, resourceID.resourceURI.toString(), 'text/turtle')
    const candidate = new FactCandidate(store, resourceID);
    const authority = await service.getLastAuthorityRevision(idFactory.createAuthorityResourceID(authorityID));
    candidate.attributeToAuthority(authority.resourceID);
    if(isFact) {
        const fact = (await service.getLastFactRevision(resourceID))
        candidate.wasRevisionOf(fact);
        return candidate
    }
    else {
        return candidate
    }
}

export async function createBinaryResourceCandidate(path:string) {
    const config = JSON.parse(fs.readFileSync(pat.join(__dirname,'/config.json')));
    const authorityID = config.ldpAddress;
    const mountpoint = config.mountpoint;

    const extension = pat.extname(path)
    path = path.replace(extension,"")
    const resourceID = idFactory.createResourceID(authorityID, path);
    const isFact = await service.isResourceExistent(resourceID);
    if(!isFact) {
        console.log("**** is not a fact already, creating empty binary candidate ******")
        console.log(mime.lookup(extension))
        const candidate = service.getFactDagResourceFactory().createEmptyBinaryFactCandidate(resourceID, fs.createReadStream(mountpoint+path+extension), mime.lookup(extension));
        candidate.attributeToAuthority(idFactory.createAuthorityResourceID(authorityID));
        return candidate;
    }
    else {
        console.log("**** IS a binary fact already ******")

        const fact = (await service.getLastBinaryFactRevision(resourceID,""))
        const candidate = fact.createBinaryCandidate()
        candidate.binaryContent = fs.createReadStream(mountpoint+path+extension);
        candidate.attributeToAuthority(idFactory.createAuthorityResourceID(authorityID));
        console.log(candidate.binaryContent)


        return candidate;
    }
}

/*export async function createBinaryResourceCandidate(path:string) { //alternate binary creation
    const config = JSON.parse(fs.readFileSync(pat.join(__dirname,'/config.json')));
    const authorityID = config.ldpAddress;
    const mountpoint = config.mountpoint;
    const extension = pat.extname(path)
    path = path.replace(extension,"")

    const resourceID = idFactory.createResourceID(authorityID, path);
    const isFact = await service.isResourceExistent(resourceID);

    const candidate = service.getFactDagResourceFactory().createEmptyBinaryFactCandidate(resourceID, fs.createReadStream(mountpoint+path+extension), mime.lookup(extension));
    candidate.attributeToAuthority(idFactory.createAuthorityResourceID(authorityID));

    if(isFact) {
        const fact = (await service.getLastFactRevision(resourceID))
        candidate.wasRevisionOf(fact);
        return candidate
    }
    else {
        return candidate
    }
}*/