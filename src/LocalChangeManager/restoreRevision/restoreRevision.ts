import {LdpMementoClient,
    ProcessLifecycleManager,
    } from "@i5/factlib.js/index";
import { IFactID } from "@i5/factlib.js/src/datamodels/interfaces/IFactID";

const service = new LdpMementoClient();
const fs = require('fs')
const pat = require('path')

export async function restoreRevision(revisionID:IFactID) {
    const config = JSON.parse(fs.readFileSync(pat.join(__dirname,'/config.json')));
    const restorationConfig = {
      "authority": {
        "uri": revisionID.authorityID
      },
      "process": {
        "checkLatest": false,
        "location": "/processes/"+config.userID,
        "uses": [],
        "generated": []
      },
      "activity": {
        "location": "/activities/restoration"
      }
    }
    
    const pManager = new ProcessLifecycleManager(restorationConfig);
    await pManager.start()
    const revision = await service.getSpecificFact(revisionID);
    const current = (await service.getLastFactRevision(revisionID)).createCandidate();
    const candidate = revision.createCandidate();
    candidate.trunk = current.trunk
    await pManager.execProcess([candidate], undefined, "Restored from revision " + revisionID.revisionID);
}